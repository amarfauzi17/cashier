<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return redirect('/login');
});

Auth::routes(['register' => false]);

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['as' => 'dashboard.', 'prefix' => 'dashboard', 'middleware' => ['auth']], function (){
	Route::get('/', [App\Http\Controllers\Backend\DashboardController::class, 'index'])->name('index');
	Route::resource('/barang', App\Http\Controllers\Backend\ProductController::class, ['names' => 'product'])->except('show');
	Route::resource('/stok', App\Http\Controllers\Backend\StockController::class, ['names' => 'stock'])->except('edit', 'create', 'show');
	Route::resource('/satuan', App\Http\Controllers\Backend\UnitController::class, ['names' => 'unit'])->except('edit', 'create', 'show');
	Route::resource('/kategori', App\Http\Controllers\Backend\CategoryController::class, ['names' => 'category'])->except('edit', 'create', 'show');
	Route::resource('/supplier', App\Http\Controllers\Backend\SupplierController::class, ['names' => 'supplier'])->except('edit', 'create', 'show');
	Route::resource('/customer', App\Http\Controllers\Backend\CustomerController::class, ['names' => 'customer'])->except('edit', 'create', 'show');
	Route::get('/kasir/receipt/{id}', [App\Http\Controllers\Backend\OrderController::class, 'receipt'])->name('order.receipt');
	Route::get('/kasir/transaksi/{id?}', [App\Http\Controllers\Backend\OrderController::class, 'show'])->name('order.show');
	// Route::put('/kasir/transaksi', [App\Http\Controllers\Backend\OrderController::class, 'transaction'])->name('order.transaction');
	Route::resource('/kasir', App\Http\Controllers\Backend\OrderController::class, ['names' => 'order'])->except('edit', 'create', 'show');

	Route::get('/report/stok', [App\Http\Controllers\Backend\ReportController::class, 'stockIndex'])->name('report.stock-index');
	Route::get('/report/penjualan', [App\Http\Controllers\Backend\ReportController::class, 'orderIndex'])->name('report.order-index');
	Route::get('/report/stok/data', [App\Http\Controllers\Backend\ReportController::class, 'stockShow'])->name('report.stock-show');
	Route::get('/report/penjualan/data', [App\Http\Controllers\Backend\ReportController::class, 'orderShow'])->name('report.order-show');
	Route::get('/report/barang/data', [App\Http\Controllers\Backend\ReportController::class, 'productShow'])->name('report.product-show');

	Route::resource('/hak-akses', App\Http\Controllers\Backend\RoleController::class, ['names' => 'role'])->except('create', 'edit', 'show');
	Route::put('/user/reset-password/{id}', [App\Http\Controllers\Backend\UserController::class, 'resetPassword'])->name('user.reset-password');
	Route::resource('/user', App\Http\Controllers\Backend\UserController::class, ['names' => 'user'])->except('create', 'edit', 'show');

	Route::resource('/setting', App\Http\Controllers\Backend\SettingController::class, ['names' => 'setting'])->except('edit', 'create', 'show');
	Route::post('setting/password', [App\Http\Controllers\Backend\SettingController::class, 'password'])->name('setting.password');

	Route::get('/404', function(){
		return '404';
	})->name('error.404');
});
