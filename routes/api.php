<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
	return $request->user();
});

Route::group(['as' => 'api.'], function (){
	Route::get('/product/search', [App\Http\Controllers\Api\StockController::class, 'searchProduct'])->name('stock.search-product');
	Route::post('/order/data', [App\Http\Controllers\Api\OrderController::class, 'data'])->name('order.data');
	Route::post('/order/stock', [App\Http\Controllers\Api\OrderController::class, 'stock'])->name('order.stock');
	Route::post('/order/discount', [App\Http\Controllers\Api\OrderController::class, 'discount'])->name('order.discount');
	Route::post('/order/paid', [App\Http\Controllers\Api\OrderController::class, 'paid'])->name('order.paid');
	Route::get('/order/search', [App\Http\Controllers\Api\OrderController::class, 'searchProduct'])->name('order.search-product');
	Route::delete('/order/destroy', [App\Http\Controllers\Api\OrderController::class, 'destroy'])->name('order.destroy');
	Route::get('/product/{id}', [App\Http\Controllers\Api\StockController::class, 'edit'])->name('stock.edit');
	Route::get('/supplier/{id}', [App\Http\Controllers\Api\SupplierController::class, 'edit'])->name('supplier.edit');
	Route::get('/customer/{id}', [App\Http\Controllers\Api\CustomerController::class, 'edit'])->name('customer.edit');
});