@extends('layouts.backend')
@section('css')
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet">
<style type="text/css">
	.select2-container{
		width: 100% !important;
	}
	.select2-container .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
		height: 34px !important;
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 34px !important;
	}
</style>
@endsection
@section('content')
<div class="card shadow mb-4">
	<div class="card-header py-3">
		@can('create stok')
		<a data-target="#modal-create" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Stok</a>
		@endcan
		<h6 class="m-0 font-weight-bold text-primary float-right">List Tabel Stok Barang</h6>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th width="5%">No</th>
						<th>Barang</th>
						<th>Stok</th>
						<th>Supplier</th>
						<th>Keterangan</th>
						<th>Tanggal Masuk</th>
						@canany(['edit stok', 'delete stok'])
						<th><center>#</center></th>
						@endcan
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th width="5%">No</th>
						<th>Barang</th>
						<th>Stok</th>
						<th>Supplier</th>
						<th>Keterangan</th>
						<th>Tanggal Masuk</th>
						@canany(['edit stok', 'delete stok'])
						<th><center>#</center></th>
						@endcan
					</tr>
				</tfoot>
				<tbody>
					@foreach($stocks as $stock)
					<tr>
						<td>{{$loop->iteration}}</td>
						<td>{!!$stock->product->name."<br>".$stock->product->id!!}</td>
						<td><center><b>{{$stock->stock}}</b></center></td>
						<td>{{$stock->supplier->name}}</td>
						<td>{{$stock->note}}</td>
						<td>{{date('d-m-Y H:i', strtotime($stock->created_at))}}</td>
						@canany(['edit stok', 'delete stok'])
						<td>
							<center>
								@can('edit stok')
								<a onclick="modalEdit(this)" data-id="{{$stock->id}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
								@endcan
								@can('delete stok')
								<a data-toggle="modal" onclick="modalDelete(this)" data-id="{{$stock->id}}" data-name="{{$stock->product->name}}" data-target="#modal-delete" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
								@endcan
							</center>
						</td>
						@endcan
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@can('create stok')
<div class="modal fade" id="modal-create" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="{{route('dashboard.stock.store')}}" method="POST">
				@csrf
				<div class="modal-header bg-primary">
					<h5 class="modal-title text-white">Tambah Stok Barang</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama Barang</label>
						<select class="select2 form-control w-100" name="product_id" required></select>
					</div>
					<div class="form-group">
						<label id="text-satuan">Stok Barang</label>
						<input type="number" name="stock" class="form-control" min="1" placeholder="masukan stok barang" required>
					</div>
					<div class="form-group">
						<label>Supplier</label>
						<select class="form-control" name="supplier_id" required>
							<option value="">Pilih Suplier</option>
							@foreach($suppliers as $supplier)
							<option value="{{$supplier->id}}">{{$supplier->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label id="text-satuan">Keterangan</label>
						<textarea class="form-control" rows="3" name="note"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('edit stok')
<div class="modal fade" id="modal-edit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('PUT')
				<div class="modal-header bg-primary">
					<h5 class="modal-title text-white">Edit Stok Barang</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama Barang</label>
						<select class="select2 form-control w-100" name="product_id" required></select>
					</div>
					<div class="form-group">
						<label id="text-satuan">Stok Barang</label>
						<input type="number" name="stock" class="form-control" min="1" placeholder="masukan stok barang" required>
					</div>
					<div class="form-group">
						<label>Supplier</label>
						<select class="form-control" name="supplier_id" required>
							<option value="">Pilih Suplier</option>
							@foreach($suppliers as $supplier)
							<option value="{{$supplier->id}}">{{$supplier->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label id="text-satuan">Keterangan</label>
						<textarea class="form-control" rows="3" name="note"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Edit</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('delete stok')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h5 class="modal-title text-white">Hapus Barang</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Apakah ingin menghapus stok barang <span id="modal-delete-name"></span> ?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan
@endsection
@push('js')
<script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#dataTable').DataTable();
	});

	@can('delete stok')
	function modalDelete(self)
	{
		var modal = $('#modal-delete');
		var id 	  = $(self).data('id');
		var name  = $(self).data('name');
		var url   = '{{route('dashboard.stock.destroy', ':id')}}';
		url = url.replace(':id', id);
		$('#modal-delete-name').html('<b>'+name+'</b>');
		modal.find('form').attr('action', url);
	}
	@endcan

	@can('edit stok')
	function modalEdit(self)
	{
		var modal = $('#modal-edit');
		var id  = $(self).data('id');
		var url = '{{route('api.stock.edit', ':id')}}';
		url = url.replace(':id', id);
		$.get(url, function(response){
			if (response.status === 200) {
				var data = response.data;
				modal.find('[name="product_id"]').append('<option value="'+data.product_id+'">'+data.product_id+" - "+data.name+'</option>').change();
				modal.find('#text-satuan').html('Stok Barang dalam '+data.unit);
				modal.find('[name="stock"]').val(data.stock);
				modal.find('[name="supplier_id"]').val(data.supplier_id);
				modal.find('[name="note"]').val(data.note);
			}
		});
		var url = '{{route('dashboard.stock.update', ':id')}}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.modal('show');
	}
	@endcan

	@canany(['create stok', 'edit stok'])
	$('.select2').select2({
		placeholder: 'Masukan nama barang atau kode barang',
		ajax: {
			url: '{{route('api.stock.search-product')}}',
			dataType: 'json',
			delay: 250,
			processResults: function (data) {
				return {
					results:  $.map(data, function (item) {
						return {
							text: item.id+' - '+item.name,
							id: item.id,
							satuan: item.satuan
						}
					})
				};
			},
			cache: true
		},
		minimumInputLength: 3,
	}).on('change', function(){
		var satuan = $(this).select2('data')[0].satuan;
		if (satuan != 'undefined') {
			$('#text-satuan').html('Stok Barang dalam '+satuan);			
		}
	});
	@endcan
</script>
@endpush