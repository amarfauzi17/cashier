@extends('layouts.backend')
@section('css')
<style type="text/css">
	.file {
		visibility: hidden;
		position: absolute;
	}
</style>
@endsection
@section('content')
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Ganti Password</h6>
	</div>
	<form action="{{route('dashboard.setting.password')}}" method="POST">
		@csrf
		<div class="card-body">
			<div class="form-group">
				<label>Password Lama</label>
				<input type="password" name="cpassword" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Password Baru</label>
				<input type="password" name="npassword" class="form-control" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Ulangi Password baru</label>
				<input type="password" name="rpassword" class="form-control" autocomplete="off" required>
			</div>
		</div>
		<div class="card-footer">
			<button type="button" onclick="location.href='{{url()->previous()}}'" class="btn btn-danger">Back</button>
			<button type="submit" class="btn btn-primary float-right">Update</button>
		</div>
	</form>
</div>
@endsection