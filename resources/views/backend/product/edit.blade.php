@extends('layouts.backend')
@section('css')
<style type="text/css">
	.file {
		visibility: hidden;
		position: absolute;
	}
</style>
@endsection
@section('content')
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Update Barang</h6>
	</div>
	<form action="{{route('dashboard.product.update', $product->id)}}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('PUT')
		<div class="card-body">
			<div class="form-group">
				<label>Nama Barang</label>
				<input type="text" name="name" class="form-control" value="{{old('name', $product->name)}}" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Photo Barang</label>
				<input type="file" name="thumbnail" class="file" accept="image/*">
				<div class="input-group">
					<input type="text" class="form-control" disabled placeholder="Upload File" id="file">
					<div class="input-group-append">
						<button type="button" class="browse btn btn-primary">Change...</button>
					</div>
				</div>
				<div class="ml-2 col-sm-12 col-lg-4 mt-2 {{empty($product->thumbnail) ? 'd-none' : ''}}">
					<img src="{{asset('img/barang/'.$product->thumbnail)}}" id="preview" class="img-thumbnail" style="max-width: 150px">
				</div>
			</div>
			<div class="form-group">
				<label>Kategori Barang</label>
				<select class="form-control" name="category_id" required>
					<option value="">Pilih Kategori</option>
					@foreach($categories as $category)
					<option value="{{$category->id}}" {{$product->category_id === $category->id ? 'selected' : ''}}>{{$category->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label>Harag Beli / <span>{{$product->unit->name ?? 'Satuan'}}</span></label>
				<input type="text" name="purchase_price" autocomplete="off" onkeyup="formatRupiah(this)" value="Rp. {{old('purchase_price', rupiah($product->purchase_price))}}" placeholder="Rp. " class="form-control" required>
			</div>
			<div class="form-group">
				<label>Harag Jual / <span>{{$product->unit->name ?? 'Satuan'}}</span></label>
				<input type="text" name="selling_price" autocomplete="off" onkeyup="formatRupiah(this)" value="Rp. {{old('selling_price', rupiah($product->selling_price))}}" placeholder="Rp. " class="form-control" required>
			</div>
		</div>
		<div class="card-footer">
			<button type="button" onclick="location.href='{{url()->previous()}}'" class="btn btn-danger">Back</button>
			<button type="submit" class="btn btn-primary float-right">Submit</button>
		</div>
	</form>
</div>
@endsection
@push('js')
<script type="text/javascript">
	function formatRupiah(self)
	{
		var angka = $(self).val();
		angka     = angka.replace(/[^,\d]/g, '').toString(),
		split     = angka.split(','),
		sisa      = split[0].length % 3,
		rupiah    = split[0].substr(0, sisa),
		ribuan    = split[0].substr(sisa).match(/\d{3}/gi);
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}
		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		$(self).val('Rp. '+rupiah);
	}

	$(document).on("click", ".browse", function() {
		var file = $(this).parents().find(".file");
		file.trigger("click");
	});
	$('input[type="file"]').change(function(e) {
		var preview  = $('#preview');
		var fileName = e.target.files[0].name;
		$("#file").val(fileName);

		var reader = new FileReader();
		reader.onload = function(e) {
			preview.attr('src', e.target.result);
			preview.parent('div.d-none').removeClass('d-none');
		};
		reader.readAsDataURL(this.files[0]);
	});
</script>
@endpush