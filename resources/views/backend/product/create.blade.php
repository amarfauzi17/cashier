@extends('layouts.backend')
@section('css')
<style type="text/css">
	.file {
		visibility: hidden;
		position: absolute;
	}
</style>
@endsection
@section('content')
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Tambah Barang</h6>
	</div>
	<form action="{{route('dashboard.product.store')}}" method="POST" enctype="multipart/form-data">
		@csrf
		<div class="card-body">
			<div class="form-group">
				<label>Nama Barang</label>
				<input type="text" name="name" class="form-control" value="{{old('name')}}" autocomplete="off" required>
			</div>
			<div class="form-group">
				<label>Photo Barang</label>
				<input type="file" name="thumbnail" class="file" accept="image/*">
				<div class="input-group">
					<input type="text" class="form-control" disabled placeholder="Upload File" id="file">
					<div class="input-group-append">
						<button type="button" class="browse btn btn-primary">Browse...</button>
					</div>
				</div>
				<div class="ml-2 col-sm-12 col-lg-4 mt-2 d-none">
					<img src="https://placehold.it/80x80" id="preview" class="img-thumbnail" style="max-width: 150px">
				</div>
			</div>
			<div class="form-group">
				<label>Kategori Barang</label>
				<select class="form-control" name="category_id" required>
					<option value="">Pilih Kategori</option>
					@foreach($categories as $category)
					<option value="{{$category->id}}">{{$category->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label>Stok Barang</label>
				<div class="row">
					<div class="col-lg-8 col-6">
						<input type="number" min="0" value="0" name="stock" value="{{old('stock')}}" autocomplete="off" required class="form-control">
					</div>
					<div class="col-lg-4 col-6">
						<select class="form-control" name="unit_id" onchange="setSatuan(this)" required>
							<option value="">Pilih Satuan</option>
							@foreach($units as $unit)
							<option value="{{$unit->id}}">{{$unit->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label>Supplier</label>
				<select class="form-control" name="supplier" required>
					<option value="">Pilih Supplier</option>
					@foreach($suppliers as $supplier)
					<option value="{{$supplier->id}}">{{$supplier->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label>Harag Beli / <span class="text-satuan">Satuan</span></label>
				<input type="text" name="purchase_price" autocomplete="off" onkeyup="formatRupiah(this)" value="{{old('purchase_price')}}" placeholder="Rp. " class="form-control" required>
			</div>
			<div class="form-group">
				<label>Harag Jual / <span class="text-satuan">Satuan</span></label>
				<input type="text" name="selling_price" autocomplete="off" onkeyup="formatRupiah(this)" value="{{old('selling_price')}}" placeholder="Rp. " class="form-control" required>
			</div>
		</div>
		<div class="card-footer">
			<button type="button" onclick="location.href='{{url()->previous()}}'" class="btn btn-danger">Back</button>
			<button type="submit" class="btn btn-primary float-right">Submit</button>
		</div>
	</form>
</div>
@endsection
@push('js')
<script type="text/javascript">
	$(document).on("click", ".browse", function() {
		var file = $(this).parents().find(".file");
		file.trigger("click");
	});
	
	$('input[type="file"]').change(function(e) {
		var preview  = $('#preview');
		var fileName = e.target.files[0].name;
		$("#file").val(fileName);

		var reader = new FileReader();
		reader.onload = function(e) {
			preview.attr('src', e.target.result);
			preview.parent('div.d-none').removeClass('d-none');
		};
		reader.readAsDataURL(this.files[0]);
	});

	function setSatuan(self)
	{
		if ($(self).val() != '') {
			var text = $(self).find('option:selected').text();
			$('.text-satuan').html(text);
		}
	}

	function formatRupiah(self)
	{
		var angka = $(self).val();
		angka     = angka.replace(/[^,\d]/g, '').toString(),
		split     = angka.split(','),
		sisa      = split[0].length % 3,
		rupiah    = split[0].substr(0, sisa),
		ribuan    = split[0].substr(sisa).match(/\d{3}/gi);
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}
		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		$(self).val('Rp. '+rupiah);
	}

</script>
@endpush