@extends('layouts.backend')
@section('css')
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<style type="text/css">
	.img-preview{
		max-height: 100px;
		max-width: 100px;
		object-fit: contain;
	}
	.modal-img{
		pointer-events: auto !important;
	}
</style>
@endsection
@section('content')
<div class="card shadow mb-4">
	<div class="card-header py-3">
		@can('create barang')
		<a href="{{route('dashboard.product.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data Barang</a>
		&nbsp;&nbsp;&nbsp;&nbsp;
		@endcan
		<h6 class="card-title float-right">List Tabel Barang</h6>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th width="5%">No</th>
						{{-- <th>Kode</th> --}}
						<th>Barang</th>
						<th>Photo</th>
						<th>Kategori</th>
						<th>Stok</th>
						<th>Harga Beli</th>
						<th>Harga Jual</th>
						@canany(['edit barang', 'delete barang'])
						<th><center>#</center></th>
						@endcan
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th width="5%">No</th>
						{{-- <th>Kode</th> --}}
						<th>Barang</th>
						<th>Photo</th>
						<th>Kategori</th>
						<th>Stok</th>
						<th>Harga Beli</th>
						<th>Harga Jual</th>
						@canany(['edit barang', 'delete barang'])
						<th><center>#</center></th>
						@endcan
					</tr>
				</tfoot>
				<tbody>
					@foreach($products as $product)
					<tr>
						<td>{{$loop->iteration}}</td>
						<td>{!!$product->name.'<br>'.$product->id!!}</td>
						<td>
							@if(!empty($product->thumbnail))
							<a data-toggle="modal" onclick="modalPreview(this)" data-img="{{asset('img/barang/'.$product->thumbnail)}}" data-target="#modal-preview-img" class="modal-img btn">
								<img data-original="{{asset('img/barang/'.$product->thumbnail)}}" class="img-preview">
							</a>
							@else
							<center><i>---</i></center>
							@endif
						</td>
						<td>{{$product->category->name ?? ''}}</td>
						<td>{{$product->current_stock ?? 0}}</td>
						<td>{{rupiah($product->purchase_price).' / '.$product->unit->name }}</td>
						<td>{{rupiah($product->selling_price).' / '.$product->unit->name }}</td>
						@canany(['edit barang', 'delete barang'])
						<td>
							<center>
								@can('edit barang')
								<a href="{{route('dashboard.product.edit', $product->id)}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
								@endcan
								@can('delete barang')
								<a data-toggle="modal" onclick="modalDelete(this)" data-id="{{$product->id}}" data-name="{{$product->name}}" data-target="#modal-delete" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
								@endcan
							</center>
						</td>
						@endcan
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-preview-img" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white">Preview Photo Barang</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img src="" class="img-fluid">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

@can('delete barang')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h5 class="modal-title text-white">Hapus Barang</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Apakah ingin menghapus barang <span id="modal-delete-name"></span> ?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@endsection
@push('js')
<script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('js/jquery.lazyload.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#dataTable').DataTable();
	});

	$("img").lazyload({
		effect : "fadeIn"
	});

	@can('delete barang')
	function modalDelete(self)
	{
		var modal = $('#modal-delete');
		var id 	  = $(self).data('id');
		var name  = $(self).data('name');
		var url   = '{{route('dashboard.product.destroy', ':id')}}';
		url = url.replace(':id', id);
		$('#modal-delete-name').html('<b>'+name+'</b>');
		modal.find('form').attr('action', url);
	}
	@endcan

	function modalPreview(self)
	{
		var img   = $(self).data('img');
		var modal = $('#modal-preview-img');
		modal.find('img').attr('src', img);
	}
</script>
@endpush