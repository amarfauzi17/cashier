@extends('layouts.backend')
@section('css')
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="card shadow mb-4">
	<div class="card-header py-3">
		@can('create supplier')
		<a data-target="#modal-create" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Supplier Barang</a>
		@endcan
		<h6 class="m-0 font-weight-bold text-primary float-right">List Tabel Supplier Barang</h6>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th width="5%">No</th>
						<th>Nama</th>
						<th>No HP</th>
						<th>Email</th>
						<th>Alamat</th>
						@canany(['delete supplier', 'edit supplier'])
						<th width="10%"><center>#</center></th>
						@endcan
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th width="5%">No</th>
						<th>Nama</th>
						<th>No HP</th>
						<th>Email</th>
						<th>Alamat</th>
						@canany(['delete supplier', 'edit supplier'])
						<th width="10%"><center>#</center></th>
						@endcan
					</tr>
				</tfoot>
				<tbody>
					@foreach($suppliers as $supplier)
					<tr>
						<td>{{$loop->iteration}}</td>
						<td>{{$supplier->name}}</td>
						<td>{{$supplier->phone}}</td>
						<td>{{$supplier->email}}</td>
						<td>{{$supplier->address}}</td>
						@canany(['delete supplier', 'edit supplier'])
						<td>
							<center>
								@can('edit supplier')
								<a onclick="modalEdit(this)" data-id="{{$supplier->id}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
								@endcan
								@can('delete supplier')
								<a data-toggle="modal" onclick="modalDelete(this)" data-id="{{$supplier->id}}" data-name="{{$supplier->name}}" data-target="#modal-delete" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
								@endcan
							</center>
						</td>
						@endcan
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@can('create supplier')
<div class="modal fade" id="modal-create" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="{{route('dashboard.supplier.store')}}" method="POST">
				@csrf
				<div class="modal-header bg-primary">
					<h5 class="modal-title text-white">Tambah Supplier Barang</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama Supplier</label>
						<input type="text" name="name" class="form-control" required>
					</div>
					<div class="form-group">
						<label>No HP</label>
						<input type="text" name="phone" minlength="8" maxlength="15" onkeypress="return isNumberKey(event)" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="email" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Alamat</label>
						<textarea class="form-control" name="address" rows="3" required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('edit supplier')
<div class="modal fade" id="modal-edit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('PUT')
				<div class="modal-header bg-primary">
					<h5 class="modal-title text-white">Edit Supplier Barang</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama Supplier</label>
						<input type="text" name="name" class="form-control" required>
					</div>
					<div class="form-group">
						<label>No HP</label>
						<input type="text" name="phone" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" name="email" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Alamat</label>
						<textarea class="form-control" name="address" rows="3" required></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Edit</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('delete supplier')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h5 class="modal-title text-white">Hapus Supplier Barang</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Apakah ingin menghapus supplier <span id="modal-delete-name"></span> ?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan
@endsection
@push('js')
<script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#dataTable').DataTable();
	});

	function isNumberKey(evt){
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}

	@can('delete supplier')
	function modalDelete(self)
	{
		var modal = $('#modal-delete');
		var id 	  = $(self).data('id');
		var name  = $(self).data('name');
		var url   = '{{route('dashboard.supplier.destroy', ':id')}}';
		url = url.replace(':id', id);
		$('#modal-delete-name').html('<b>'+name+'</b>');
		modal.find('form').attr('action', url);
	}
	@endcan

	@can('edit supplier')
	function modalEdit(self)
	{
		var modal  = $('#modal-edit');
		var id     = $(self).data('id');
		var apiUrl = '{{route('api.supplier.edit', ':id')}}';
		apiUrl     = apiUrl.replace(':id', id)
		$.get(apiUrl, function(response){
			if (response.status === 200) {
				var data = response.data;
				modal.find('input[name="name"]').val(data.name);
				modal.find('input[name="phone"]').val(data.phone);
				modal.find('input[name="email"]').val(data.email);
				modal.find('textarea[name="address"]').val(data.address);
			}
		});

		var url   = '{{route('dashboard.supplier.update', ':id')}}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.modal('show');
	}
	@endcan
</script>
@endpush