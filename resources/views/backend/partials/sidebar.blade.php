<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{set_active('dashboard.index')}}">
        <a class="nav-link" href="{{route('dashboard.index')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Nav Item - Pages Collapse Menu -->
    @canany(['view transaksi', 'create transaksi', 'delete transaksi'])
    <li class="nav-item {{set_active('dashboard.order.show')}}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCashier" aria-expanded="true" aria-controls="collapseCashier">
            <i class="fas fa-cash-register"></i>
            <span>Kasir</span>
        </a>
        <div id="collapseCashier" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{route('dashboard.order.show')}}">Transasksi</a>
                <a class="collapse-item" href="{{route('dashboard.order.index')}}">Histori</a>
            </div>
        </div>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    @endcan
    <!-- Heading -->
    @canany(['view kategori', 'create kategori', 'edit kategori', 'delete kategori', 'view satuan', 'create satuan', 'edit satuan', 'delete satuan', 'view supplier', 'create supplier', 'edit supplier', 'delete supplier', 'view barang', 'create barang', 'edit barang', 'delete barang', 'view customer', 'create customer', 'edit customer', 'delete customer'])
    <div class="sidebar-heading"></div>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{set_active(['dashboard.category.index', 'dashboard.unit.index', 'dashboard.supplier.index', 'dashboard.product.index', 'dashboard.product.create', 'dashboard.product.edit', 'dashboard.customer.index'])}}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseMaster" aria-expanded="true" aria-controls="collapseMaster">
            <i class="fas fa-box"></i>
            <span>Master</span>
        </a>
        <div id="collapseMaster" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @canany(['view kategori', 'create kategori', 'edit kategori', 'delete kategori'])
                <a class="collapse-item" href="{{route('dashboard.category.index')}}">Kategori Barang</a>
                @endcan
                @canany(['view satuan', 'create satuan', 'edit satuan', 'delete satuan'])
                <a class="collapse-item" href="{{route('dashboard.unit.index')}}">Satuan Barang</a>
                @endcan
                @canany(['view supplier', 'create supplier', 'edit supplier', 'delete supplier'])
                <a class="collapse-item" href="{{route('dashboard.supplier.index')}}">Supplier</a>
                @endcan
                @canany(['view barang', 'create barang', 'edit barang', 'delete barang'])
                <a class="collapse-item" href="{{route('dashboard.product.index')}}">Barang</a>
                @endcan
                @canany(['view customer', 'create customer', 'edit customer', 'delete customer'])
                <a class="collapse-item" href="{{route('dashboard.customer.index')}}">Pelanggan</a>
                @endcan
            </div>
        </div>
    </li>
    @endcan
    <!-- Nav Item - Charts -->
    @canany(['view stok', 'create stok', 'edit stok', 'delete stok'])
    <li class="nav-item">
        <a class="nav-link" href="{{route('dashboard.stock.index')}}">
            <i class="fa fa-cube"></i>
            <span>Stok Barang</span>
        </a>
    </li>
    @endcan
    <!-- Divider -->
    @canany(['view laporan stok', 'view laporan transaksi', 'view laporan barang'])
    <hr class="sidebar-divider d-none d-md-block">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseReport" aria-expanded="true" aria-controls="collapseReport">
            <i class="fas fa-fw fa-folder"></i>
            <span>Laporan</span>
        </a>
        <div id="collapseReport" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @can('view laporan transaksi')
                <a class="collapse-item" href="{{route('dashboard.report.order-index')}}">Penjualan</a>
                @endcan
                @can('view laporan stok')
                <a class="collapse-item" href="{{route('dashboard.report.stock-index')}}">Barang Masuk</a>
                @endcan
                @can('view laporan barang')
                <a class="collapse-item" href="{{route('dashboard.report.product-show')}}" target="_blank">Data Barang</a>
                @endcan
            </div>
        </div>
    </li>
    @endcan
    @canany(['view hak akses', 'create hak akses', 'edit hak akses', 'delete hak akses', 'view user', 'create user', 'edit user', 'delete user', 'reset password user'])
    <hr class="sidebar-divider d-none d-md-block">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUser" aria-expanded="true" aria-controls="collapseUser">
            <i class="fas fa-fw fa-user"></i>
            <span>User Management</span>
        </a>
        <div id="collapseUser" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @canany(['view hak akses', 'create hak akses', 'edit hak akses', 'delete hak akses'])
                <a class="collapse-item" href="{{route('dashboard.role.index')}}">Hak Akses</a>
                @endcan
                @canany(['view user', 'create user', 'edit user', 'delete user', 'reset password user'])
                <a class="collapse-item" href="{{route('dashboard.user.index')}}">User</a>
                @endcan
            </div>
        </div>
    </li>
    @endcan
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>