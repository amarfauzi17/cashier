@extends('layouts.backend')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{asset('vendor/autocomplete/jquery-ui.min.css')}}" rel="stylesheet">
<style type="text/css">
	.select2-container .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
		height: 34px !important;
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 34px !important;
	}
	.d-unset{
		display: unset !important;
	}
	.img-preview{
		max-height: 100px;
		max-width: 100px;
		object-fit: contain;
	}
	#data-transaction table tbody tr td, #data-transaction table tbody input, #data-transaction table tfoot tr th, #data-transaction table tfoot input, #data-transaction table tfoot span{
		font-size: 14px;
	}
	.b-red{
		border-color: red;
	}
</style>
@endsection
@section('content')
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<form method="POST" action="{{route('dashboard.order.store')}}" class="d-unset" id="form-product">
			@csrf
			@isset($noInv)
			<small>No Tagihan : {{$noInv}}</small>
			<div class="clearfix"></div>
			<input type="hidden" name="id" value="{{$noInv}}">
			@endisset
			<input type="hidden" name="product_id" id="product_id">
			<input type="text" id="barang" class="form-control w-50 d-inline-block" placeholder="masukan kode atau nama barang" {{empty($bill) ? '' : 'disabled'}}>
			&nbsp;&nbsp;
			@empty($bill->status)
			<button class="btn btn-sm btn-primary align-baseline" type="submit"><i class="fa fa-plus"></i> Tambah</button>
			@endempty
		</form>
		<div class="float-right">
			<label>Pelanggan :</label>
			<select class="form-control d-inline-block select2" onchange="setCustomer(this.value)" {{empty($bill) ? '' : 'disabled'}}>
				<option value="">Non Member</option>
				@foreach($customers as $customer)
				@if(empty($bill))
				<option value="{{$customer->id}}">{{$customer->name}}</option>
				@else
				<option value="{{$customer->id}}" {{$bill->customer_id == $customer->id ? 'selected' : ''}}>{{$customer->name}}</option>
				@endif
				@endforeach
			</select>
		</div>
	</div>
	<div id="data-transaction"></div>
</div>

<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<input type="hidden" id="modal-orderid">
			<div class="modal-header bg-danger">
				<h5 class="modal-title text-white">Hapus Barang Belanjaan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Apakah ingin menghapus barang <span id="modal-delete-name"></span> dari list belanjaan?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-danger" onclick="destroy()">Delete</button>
			</div>
		</div>
	</div>
</div>

@endsection
@push('js')
<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/autocomplete/jquery-ui.min.js')}}"></script>
<script type="text/javascript">

	$(document).ready(function(){
		$.post('{{route('api.order.data')}}', {'id': '{{$noInv}}'}, function(response){
			$('#data-transaction').html(response);
			$('#form-transaction').find('[name="_token"]').val($('meta[name="csrf-token"]').attr('content'));
		});
	});

	function modalDelete(self)
	{
		var modal = $('#modal-delete');
		var id 	  = $(self).parents('tr').data('orderid');
		var name  = $(self).data('name');
		$('#modal-delete-name').html('<b>'+name+'</b>');
		modal.find('#modal-orderid').val(id);
		modal.modal('show')
	}

	$('.select2').select2();

	$("#barang").autocomplete({
		source: '{{route('api.order.search-product')}}',
		minLength: 3,
		select: function(event, ui) {
			var form = $('#form-product');
			form.find('#product_id').val(ui.item.id);
		}
	});

	var request = null;
	function setQty(self)
	{
		$.ajaxSetup({cache: false});
		if (request != null) {
			request.abort();
			request = null;
		}
		var billId  = $('input[name="id"]').val();
		var orderId = $(self).parents('tr').data('orderid');
		var qty = $(self).val();
		request = $.ajax({
			type : "POST",
			url	 : "{{route('api.order.stock')}}",
			data : { 'qty':qty, 'order_id' : orderId, 'id' : billId},
			success: function (response) {
				$('#data-transaction').html(response);
				$('#form-transaction').find('[name="_token"]').val($('meta[name="csrf-token"]').attr('content'));
			},
			beforeSend: function () {
				if (request !== null) {
					request.abort();
				}
			}
		});
	}

	function destroy()
	{
		var modal   = $('#modal-delete');
		var orderId = modal.find('#modal-orderid').val();
		var billId  = $('input[name="id"]').val();
		$.ajax({
			type : "delete",
			url	 : "{{route('api.order.destroy')}}",
			data : { 'order_id' : orderId, 'id' : billId},
			success: function (response) {
				$('#data-transaction').html(response);
				$('#form-transaction').find('[name="_token"]').val($('meta[name="csrf-token"]').attr('content'));
			}
		});
		modal.modal('hide');
	}

	function setDiscount(discount)
	{
		$.ajaxSetup({cache: false});
		if (request != null) {
			request.abort();
			request = null;
		}
		var billId   = $('input[name="id"]').val();
		request = $.ajax({
			type : "POST",
			url	 : "{{route('api.order.discount')}}",
			data : { 'discount':discount, 'id' : billId},
			success: function (response) {
				$('#data-transaction').html(response);
				$('#form-transaction').find('[name="_token"]').val($('meta[name="csrf-token"]').attr('content'));
			},
			beforeSend: function () {
				if (request !== null) {
					request.abort();
				}
			}
		});
	}

	function setPaid(self)
	{
		$.ajaxSetup({cache: false});
		if (request != null) {
			request.abort();
			request = null;
		}
		var billId   = $('input[name="id"]').val();
		request = $.ajax({
			type : "POST",
			url	 : "{{route('api.order.paid')}}",
			data : { 'paid': $(self).val(), 'id' : billId},
			success: function (response) {
				$('#data-transaction').html(response);
				$('#form-transaction').find('[name="_token"]').val($('meta[name="csrf-token"]').attr('content'));
			},
			beforeSend: function () {
				if (request !== null) {
					request.abort();
				}
			}
		});
	}

	function setCustomer(val)
	{
		$('input[name="customer_id"]').val(val);
	}

	function setRupiahBayar(self)
	{
		$(self).val('Rp. '+formatRupiah($(self).val()));
	}

	function formatRupiah(number)
	{
		angka     = number.toString().replace(/[^,\d]/g, ''),
		split     = angka.split(','),
		sisa      = split[0].length % 3,
		rupiah    = split[0].substr(0, sisa),
		ribuan    = split[0].substr(sisa).match(/\d{3}/gi);
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}
		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return rupiah;
	}
</script>
@endpush