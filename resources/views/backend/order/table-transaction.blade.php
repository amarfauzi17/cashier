@php
$pay       = $transaction->pay ?? null;
$discount  = $transaction->discount ?? null;
$cutting_price = $pay * $discount / 100;
$total     = $pay - $cutting_price;
$pay_money = $transaction->pay_money ?? null;
$cash_back = empty($pay_money) ? null : $pay_money - $total;
$disabled  = '';
if (isset($transaction->status)) {
	$disabled = $transaction->status == 1 ? '' : 'disabled';
}
$transactionId = empty($transaction->id) ? 'null' : $transaction->id;
@endphp
<form method="POST" action="{{$cash_back < 0 ? '' : route('dashboard.order.update', $transactionId)}}" id="form-transaction">
	@csrf
	@method('PUT')
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" width="100%" cellspacing="0" data-status="{{$transaction->status ?? null}}">
				<thead>
					<tr>
						<th width="5%">No</th>
						<th>Kode</th>
						<th>Nama Barang</th>
						<th>Harga (Rp.)</th>
						<th>Qty</th>
						<th width="30%">Total</th>
						<th width="5%"><center>#</center></th>
					</tr>
				</thead>
				<tbody>
					@if(!empty($transaction->orders))
					@forelse($transaction->orders as $order)
					<tr data-orderid="{{$order->id}}">
						<td>{{$loop->iteration}}</td>
						<td>{{$order->product->id}}</td>
						<td>{{$order->product->name}}</td>
						<td>{{rupiah($order->product->selling_price)}}</td>
						<td>
							<input type="number" value="{{$order->amount}}" min="1" max="{{$order->product->current_stock + $order->amount}}" class="form-control d-inline-block" style="max-width: 60px" onchange="setQty(this)" {{$disabled}}>
							<div class="clearfix"></div>
							<small>Stok tersedia : {{$order->product->current_stock}}</small>
						</td>
						<td><span id="qtyTotal" data-price="{{$order->product->selling_price}}" data-total="{{$order->product->selling_price}}">{{rupiah($order->amount * $order->product->selling_price)}}</span></td>
						<td>
							@if($transaction->status == 1)
							<button class="btn btn-sm btn-danger" data-name="{{$order->product->name}}" type="button" onclick="modalDelete(this)"><i class="fa fa-trash"></i></button>
							@endif
						</td>
					</tr>
					@empty
					<tr class="empty">
						<td colspan="7"><center><i>kosong</i></center></td>
					</tr>
					@endforelse
					@else
					<tr class="empty">
						<td colspan="7"><center><i>kosong</i></center></td>
					</tr>
					@endif
				</tbody>
				<tfoot>
					<tr>
						<th colspan="4"></th>
						<th>Sub Total</th>
						<th><input type="text" class="form-control" value="Rp. {{rupiah($pay)}}" data-total="{{$pay}}" disabled></th>
						<th></th>
					</tr>
					<tr>
						<th colspan="4"></th>
						<th>Diskon</th>
						<th>
							<div class="input-group mb-3">
								<input type="number" min="0" max="100" value="{{$discount}}" name="discount" onchange="setDiscount(this.value)" class="form-control" placeholder="diskon dalam %" {{$disabled}}>
								<div class="input-group-append">
									<span class="input-group-text" id="basic-addon2">%</span>
								</div>
							</div>
						</th>
						<th></th>
					</tr>
					<tr>
						<th colspan="4"></th>
						<th>Potongan Diskon</th>
						<th>
							<div class="input-group mb-3">
								<div class="input-group-append">
									<span class="input-group-text" id="basic-addon2">Rp. </span>
								</div>
								<input type="text" class="form-control" id="potongan" value="{{rupiah($cutting_price) }}" disabled>
							</div>
						</th>
						<th></th>
					</tr>
					<tr>
						<th colspan="4"></th>
						<th>Total</th>
						<th><input type="text" class="form-control" value="Rp. {{rupiah($total)}}" disabled></th>
						<th></th>
					</tr>
					<tr>
						<th colspan="4"></th>
						<th>Bayar</th>
						<th><input type="text" name="bill" class="form-control" onkeyup="setRupiahBayar(this)" onchange="setPaid(this)" value="{{empty($pay_money) ? null : 'Rp. '.rupiah($pay_money)}}" {{empty($disabled) ? 'required' : $disabled}}></th>
						<th></th>
					</tr>
					<tr>
						<th colspan="4"></th>
						<th>Kembalian</th>
						<th><input type="text" name="cash_back" class="form-control{{$cash_back < 0 ? ' b-red' : ''}}" value="{{empty($cash_back) ? null : 'Rp. '.rupiah($cash_back)}}" disabled></th>
						<th></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<div class="card-footer" style="display: flow-root">
		@if(isset($transaction->status))
		@if($transaction->status == 1)
		<input type="hidden" name="customer_id">
		<button type="submit" class="btn btn-primary float-right" {{$cash_back < 0 ? 'disabled' : ''}}><i class="fas fa-cash-register"></i> Transaksi</button>
		@else
		<div class="float-right">	
			<a class="btn btn-danger" href="{{route('dashboard.order.show')}}"><i class="fas fa-cash-register"></i> Transaksi Baru</a>
			&nbsp;&nbsp;&nbsp;
			<button type="button" class="btn btn-success" onclick="window.open('{{route('dashboard.order.receipt', $transaction->id)}}','mywindow','width=400px,height=600px,left=300px;top=100px;')"><i class="fa fa-print"></i> Cetak Struk</button>
		</div>
		@endif
		@else
		<a class="btn btn-danger" href="{{url()->previous()}}">Back</a>
		@endif
	</div>
</form>