@extends('layouts.backend')
@section('css')
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Histori Transaksi</h6>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th width="5%">No</th>
						<th>No Struk</th>
						<th>Customer</th>
						<th>Total Bayar</th>
						<th>Tanggal</th>
						<th>Status</th>
						<th width="10%"><center>#</center></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th width="5%">No</th>
						<th>No Struk</th>
						<th>Customer</th>
						<th>Total Bayar</th>
						<th>Tanggal</th>
						<th>Status</th>
						@canany(['create transaksi', 'delete transaksi'])
						<th width="10%"><center>#</center></th>
						@endcan
					</tr>
				</tfoot>
				<tbody>
					@foreach($bills as $bill)
					<tr>
						<td>{{$loop->iteration}}</td>
						<td>{{$bill->id}}</td>
						<td>{{empty($bill->customer->name) ? 'Umum' : ucwords($bill->customer->name)}}</td>
						<td>{!!empty($bill->total_pay) ? '<center>-</center>' : 'Rp. '.rupiah($bill->total_pay)!!}</td>
						<td>{{date('d/m/y H:i', strtotime($bill->created_at))}}</td>
						<td>
							<label class="badge badge-{{$bill->status == 1 ? 'primary' : 'success'}}">{{$bill->status == 1 ? 'Draft' : 'Lunas'}}</label>
						</td>
						@canany(['create transaksi', 'delete transaksi'])
						<td>
							<center>
								@can('create transaksi')
								<a href="{{route('dashboard.order.show', $bill->id)}}" class="btn btn-sm btn-primary"><i class="fas fa-folder-open"></i></a>
								@endcan
								@can('delete transaksi')
								@if($bill->status == 1)
								<a data-toggle="modal" onclick="modalDelete(this)" data-id="{{$bill->id}}" data-target="#modal-delete" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
								@endif
								@endcan
							</center>
						</td>
						@endcan
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@can('delete transaksi')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h5 class="modal-title text-white">Hapus Transaksi Item</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Apakah ingin menghapus transaksi dengan nomor struk <span id="modal-delete-name"></span> ?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan
@endsection
@push('js')
<script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#dataTable').DataTable();
	});
	
	@can('delete transaksi')
	function modalDelete(self)
	{
		var modal = $('#modal-delete');
		var id 	  = $(self).data('id');
		var url   = '{{route('dashboard.order.destroy', ':id')}}';
		url = url.replace(':id', id);
		$('#modal-delete-name').html('<b>'+id+'</b>');
		modal.find('form').attr('action', url);
	}
	@endcan
</script>
@endpush