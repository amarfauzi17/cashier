<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Struk Belanjaan</title>
	<style type="text/css">
		.title{
			text-align: center;
		}
		.dashed{
			border-top: 1px dashed #000;
		}
		.mt-none{
			margin-top: unset;
		}
		.mb-none{
			margin-bottom: unset;
		}
		table{
			width: 100%;
		}
		.solid{
			border-top: 2px solid #000;
		}
		@media print{
			button{
				display: none;
			}
		}
	</style>
</head>
<body>
	<h4 class="title mb-none">Laramart App</h4>
	<p class="title mt-none">Jln Panunggulan no. 36B Indramayu</p>
	<hr class="dashed">
	<table border="0">
		<tr>
			<td>{{date('d/m/y H:i')}}</td>
			<td></td>
			<td align="right">Cashier</td>
			<td>:</td>
			<td align="right">{{$bill->createdBy->name ?? 'Admin'}}</td>
		</tr>
		<tr>
			<td>{{$bill->id}}</td>
			<td></td>
			<td align="right">Customer</td>
			<td>:</td>
			<td align="right">{{$bill->customer->name ?? 'Umum'}}</td>
		</tr>
		<tr>
			<td colspan="5"><div class="solid"></div></td>
		</tr>
		@foreach($bill->orders as $order)
		<tr>
			<td>{{$order->product->name}}</td>
			<td align="right">{{$order->amount}}</td>
			<td align="right">Rp. {{rupiah($order->product->selling_price)}}</td>
			<td></td>
			<td align="right">Rp. {{rupiah($order->product->selling_price * $order->amount)}}</td>
		</tr>
		@endforeach
		<tr>
			<td colspan="5"><div class="dashed"></div></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td align="right">Sub Total</td>
			<td></td>
			<td align="right">Rp. {{rupiah($bill->pay)}}</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td class="dashed" align="right">Potongan</td>
			<td class="dashed"></td>
			<td align="right" class="dashed">Rp. {{rupiah($bill->cutting_price)}}</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td class="dashed" align="right">Total</td>
			<td class="dashed"></td>
			<td align="right" class="dashed">Rp. {{rupiah($bill->total_pay)}}</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td class="dashed" align="right">Bayar</td>
			<td class="dashed"></td>
			<td align="right" class="dashed">Rp. {{rupiah($bill->pay_money)}}</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td class="dashed" align="right">Kembali</td>
			<td class="dashed"></td>
			<td align="right" class="dashed">Rp. {{rupiah($bill->pay_money - $bill->total_pay)}}</td>
		</tr>
	</table>
	<hr class="dashed">
	<h4 class="title mb-none">Terima Kasih</h4>
	<button type="button" onclick="window.print()">Print</button>
</body>
</html>