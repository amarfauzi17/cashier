@extends('layouts.backend')
@section('css')
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="card shadow mb-4">
	<div class="card-header py-3">
		@can('create kategori')
		<a data-target="#modal-create" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Kategori Barang</a>
		@endcan
		<h6 class="m-0 font-weight-bold text-primary float-right">List Tabel Kategori Barang</h6>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th width="5%">No</th>
						<th>Nama</th>
						@canany(['delete kategori', 'edit kategori'])
						<th width="10%"><center>#</center></th>
						@endcan
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th width="5%">No</th>
						<th>Nama</th>
						@canany(['delete kategori', 'edit kategori'])
						<th width="10%"><center>#</center></th>
						@endcan
					</tr>
				</tfoot>
				<tbody>
					@foreach($categories as $category)
					<tr>
						<td>{{$loop->iteration}}</td>
						<td>{{$category->name}}</td>
						@canany(['delete kategori', 'edit kategori'])
						<td>
							<center>
								@can('edit kategori')
								<a onclick="modalEdit(this)" data-id="{{$category->id}}" data-name="{{$category->name}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
								@endcan
								@can('delete kategori')
								<a data-toggle="modal" onclick="modalDelete(this)" data-id="{{$category->id}}" data-name="{{$category->name}}" data-target="#modal-delete" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
								@endcan
							</center>
						</td>
						@endcan
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@can('create kategori')
<div class="modal fade" id="modal-create" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="{{route('dashboard.category.store')}}" method="POST">
				@csrf
				<div class="modal-header bg-primary">
					<h5 class="modal-title text-white">Tambah Kategori Barang</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama Kategori Barang</label>
						<input type="text" name="name" class="form-control" required>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('edit kategori')
<div class="modal fade" id="modal-edit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('PUT')
				<div class="modal-header bg-primary">
					<h5 class="modal-title text-white">Edit Kategori Barang</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama Kategori Barang</label>
						<input type="text" name="name" class="form-control" required>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Edit</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('delete kategori')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h5 class="modal-title text-white">Hapus Kategori Barang</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Apakah ingin menghapus kategori barang <span id="modal-delete-name"></span> ?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@endsection
@push('js')
<script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#dataTable').DataTable();
	});

	@can('delete kategori')
	function modalDelete(self)
	{
		var modal = $('#modal-delete');
		var id 	  = $(self).data('id');
		var name  = $(self).data('name');
		var url   = '{{route('dashboard.category.destroy', ':id')}}';
		url = url.replace(':id', id);
		$('#modal-delete-name').html('<b>'+name+'</b>');
		modal.find('form').attr('action', url);
	}
	@endcan

	@can('edit kategori')
	function modalEdit(self)
	{
		var modal = $('#modal-edit');
		var id    = $(self).data('id');
		var name  = $(self).data('name');
		var url   = '{{route('dashboard.category.update', ':id')}}';
		url = url.replace(':id', id);
		modal.find('input[name="name"]').val(name);
		modal.find('form').attr('action', url);
		modal.modal('show');
	}
	@endcan
</script>
@endpush