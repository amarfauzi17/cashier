@extends('layouts.backend')
@section('css')
<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<style type="text/css">
	.select2-container .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
		height: 34px !important;
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 34px !important;
	}
	table tr td{
		padding: 5px;
	}
</style>
@endsection
@section('content')
<div class="card shadow mb-4">
	<form method="GET" action="{{route('dashboard.report.stock-show')}}" target="_blank">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Laporan Barang Masuk</h6>
		</div>
		<div class="card-body">
			<table border="0" class="w-100">
				<tr>
					<td align="right">Nama Barang</td>
					<td width="1%">:</td>
					<td width="80%" colspan="3">
						<select class="form-control select2" name="product">
							<option value="%">All</option>
							@foreach($products as $product)
							<option value="{{$product->id}}">{{$product->name}}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td align="right">Kategori Barang</td>
					<td>:</td>
					<td colspan="3">
						<select class="form-control select2" name="categories[]" multiple>
							@foreach($categories as $category)
							<option value="{{$category->id}}" selected>{{$category->name}}</option>
							@endforeach
						</select>
					</td>
				</tr>
				<tr>
					<td align="right">Tanggal</td>
					<td>:</td>
					<td width="39%">
						<input type="date" name="fdate" class="form-control" value="{{date('Y-m', strtotime('-2 month')).'-01'}}">
					</td>
					<td width="2%">s/d</td>
					<td width="39%">
						<input type="date" name="tdate" class="form-control" value="{{date('Y-m-d')}}">
					</td>
				</tr>
			</table>
		</div>
		<div class="card-footer">
			<a class="btn btn-danger" href="{{url()->previous()}}">Back</a>
			<button type="submit" class="btn btn-success float-right"><i class="fa fa-print"></i> Report</button>
		</div>
	</form>
</div>
@endsection
@push('js')
<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();
	});
</script>
@endpush