@extends('layouts.report')
@section('title', 'Barang Masuk')
@section('css')
<style type="text/css">
	table tr{
		border-style: dotted;
	}
</style>
@endsection
@section('content')
<table class="table w-100">
	<thead>
		<tr>
			<th>Tanggal</th>
			<th>Kode</th>
			<th>Barang</th>
			<th><center>Stok Masuk</center></th>
			<th>Supplier</th>
			<th>Keterangan</th>
		</tr>
	</thead>
	<tbody>
		@foreach($stocks as $stock)
		<tr>
			<td>{{date('d/m/Y', strtotime($stock->created_at))}}</td>
			<td>{{$stock->product->id}}</td>
			<td>{{$stock->product->name}}</td>
			<td><center>{{$stock->stock.' '.$stock->product->unit->name}}</center></td>
			<td>{{$stock->supplier->name}}</td>
			<td>{{$stock->note}}</td>
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>Tanggal</th>
			<th>Kode</th>
			<th>Barang</th>
			<th><center>Stok Masuk</center></th>
			<th>Supplier</th>
			<th>Keterangan</th>
		</tr>
	</tfoot>
</table>
@endsection
@push('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('table').DataTable( {
			dom: 'Bfrtip',
			searching: false,
			paging: false,
			sorting: false,
			ordering: false,
			buttons: [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5'
			]
		});
	});
</script>
@endpush