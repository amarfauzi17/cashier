@extends('layouts.report')
@section('title', 'Penjualan dari '.date('d/m/y', strtotime($request->fdate)).' samapai '.date('d/m/y', strtotime($request->fdate)))
@section('css')
<style type="text/css">
	table tr{
		border-style: dotted;
	}
</style>
@endsection
@section('content')
<table class="table w-100">
	<thead>
		<tr>
			<th>Tanggal</th>
			<th>No Struk</th>
			<th>Barang</th>
			<th>Qty</th>
			<th>Total Bayar</th>
			<th><center>Diskon</center></th>
			<th>Profit</th>
			<th>Customer</th>
		</tr>
	</thead>
	<tbody>
		@foreach($bills as $bill)
		<tr>
			<td>{{date('d/m/Y', strtotime($bill->created_at))}}</td>
			<td>{{$bill->id}}</td>
			<td>
				<ul>
					@foreach($bill->orders as $order)
					<li>{{$order->product->name}}</li>
					@endforeach
				</ul>
			</td>
			<td>
				<ul>
					@foreach($bill->orders as $order)
					<li>{{$order->amount.' '.$order->product->unit->name}}</li>
					@endforeach
				</ul>
			</td>
			<td>Rp. {{rupiah($bill->total_pay)}}</td>
			<td><center>{!!empty($bill->cutting_price) ? '-' : 'Rp. '.rupiah($bill->cutting_price)!!}</center></td>
			<td>Rp. {{rupiah($bill->profit)}}</td>
			<td>{{$bill->customer->name ?? 'Umum'}}</td>
		</tr>
		@php
		$tpay += $bill->total_pay;
		$tprofit += $bill->profit;
		$tdiscount += $bill->cutting_price;
		@endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th></th>
			<th></th>
			<th></th>
			<th>Total</th>
			<th>RP. {{rupiah($tpay) ?? 0}}</th>
			<th>Rp. {{rupiah($tdiscount) ?? 0}}</th>
			<th>RP. {{rupiah($tprofit) ?? 0}}</th>
			<th></th>
		</tr>
	</tfoot>
</table>
@endsection
@push('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('table').DataTable( {
			dom: 'Bfrtip',
			searching: false,
			paging: false,
			sorting: false,
			ordering: false,
			buttons: [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5',
			]
		});
	});
</script>
@endpush