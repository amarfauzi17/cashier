@extends('layouts.backend')
@section('css')
<link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<style type="text/css">
	.select2-container .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
		height: 34px !important;
	}
	.select2-container--default .select2-selection--single .select2-selection__rendered{
		line-height: 34px !important;
	}
	table tr td{
		padding: 5px;
	}
</style>
@endsection
@section('content')
<div class="card shadow mb-4">
	<form method="GET" action="{{route('dashboard.report.order-show')}}" target="_blank">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Laporan Barang Masuk</h6>
		</div>
		<div class="card-body">
			<table border="0" class="w-100">
				<tr>
					<td align="right">Pembayaran</td>
					<td>:</td>
					<td width="39%">
						<input type="text" name="minpay" class="form-control">
					</td>
					<td width="2%">Max</td>
					<td width="39%">
						<input type="text" name="maxpay" class="form-control">
					</td>
				</tr>
				<tr>
					<td align="right">Tanggal</td>
					<td>:</td>
					<td width="39%">
						<input type="date" name="fdate" class="form-control" value="{{date('Y-m', strtotime('-2 month')).'-01'}}">
					</td>
					<td width="2%">s/d</td>
					<td width="39%">
						<input type="date" name="tdate" class="form-control" value="{{date('Y-m-d')}}">
					</td>
				</tr>
				<tr>
					<td align="right">Pelanggan</td>
					<td>:</td>
					<td colspan="3">
						<select name="customer" class="form-control select2">
							<option value="%">All</option>
							@foreach($customers as $customer)
							<option value="{{$customer->id}}">{{$customer->name}}</option>
							@endforeach
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div class="card-footer">
			<a class="btn btn-danger" href="{{url()->previous()}}">Back</a>
			<button type="submit" class="btn btn-success float-right"><i class="fa fa-print"></i> Report</button>
		</div>
	</form>
</div>
@endsection
@push('js')
<script src="{{asset('vendor/select2/select2.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();
	});

	$('input[name="minpay"], input[name="maxpay"]').on('keyup', function(){
		$(this).val('Rp. '+formatRupiah($(this).val()));
	});

	function formatRupiah(number)
	{
		angka     = number.toString().replace(/[^,\d]/g, ''),
		split     = angka.split(','),
		sisa      = split[0].length % 3,
		rupiah    = split[0].substr(0, sisa),
		ribuan    = split[0].substr(sisa).match(/\d{3}/gi);
		if (ribuan) {
			separator = sisa ? '.' : '';
			rupiah += separator + ribuan.join('.');
		}
		rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
		return rupiah;
	}
</script>
@endpush