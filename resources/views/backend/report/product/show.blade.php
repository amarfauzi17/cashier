@extends('layouts.report')
@section('title', 'Data Barang pertanggal '.date('d/m/y'))
@section('css')
<style type="text/css">
	table tr{
		border-style: dotted;
	}
</style>
@endsection
@section('content')
<table class="table w-100">
	<thead>
		<tr>
			<th>Barang</th>
			<th>Kode</th>
			<th><center>Total Stok</center></th>
			<th><center>Stok Tersedia</center></th>
			<th><center>Satuan</center></th>
			<th>Terjual</th>
			<th>Harga Beli</th>
			<th>Harga Jual</th>
			{{-- <th>Total Profit</th> --}}
		</tr>
	</thead>
	<tbody>
		@foreach($products as $product)
		@php
		$profit = 0;
		if ($product->orders->count() > 0) {
			$profit = $product->orders->sum('amount') * ($product->selling_price - $product->purchase_price);
		}
		@endphp
		<tr>
			<td>{{$product->name}}</td>
			<td>{{$product->id}}</td>
			<td><center>{{$product->stocks->sum('stock')}}</center></td>
			<td><center>{{$product->current_stock}}</center></td>
			<td><center>{{$product->unit->name}}</center></td>
			<td><center>{{$product->orders->sum('amount')}}</center></td>
			<td>Rp. {{rupiah($product->purchase_price)}}</td>
			<td>Rp. {{rupiah($product->selling_price)}}</td>
			{{-- <td>Rp. {{rupiah($profit) ?? 0}}</td> --}}
		</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<th>Barang</th>
			<th>Kode</th>
			<th><center>Total Stok</center></th>
			<th><center>Stok Tersedia</center></th>
			<th><center>Satuan</center></th>
			<th>Terjual</th>
			<th>Harga Beli</th>
			<th>Harga Jual</th>
			{{-- <th>Total Profit</th> --}}
		</tr>
	</tfoot>
</table>
@endsection
@push('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('table').DataTable( {
			dom: 'Bfrtip',
			searching: false,
			paging: false,
			// sorting: false,
			// ordering: false,
			buttons: [
			'copyHtml5',
			'excelHtml5',
			'csvHtml5',
			'pdfHtml5'
			]
		});
	});
</script>
@endpush