@extends('layouts.backend')
@section('css')
<link href="{{asset('vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<style type="text/css">
	.list-roles{
		-webkit-column-count: 3;
		-moz-column-count: 3;
		column-count: 3;
		list-style-type: none;
	}
</style>
@endsection
@section('content')
<div class="card shadow mb-4">
	<div class="card-header py-3">
		@can('create hak akses')
		<a type="button" data-target="#modal-create" data-toggle="modal" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Hak Akses</a>
		&nbsp;&nbsp;&nbsp;&nbsp;
		@endcan
		<h6 class="card-title float-right">DataTable Hak Akses</h6>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<th width="5%">No</th>
					<th>Role</th>
					<th>Permission</th>
					@canany(['edit hak akses', 'delete hak akses'])
					<th width="5%">Action</th>
					@endcan
				</thead>
				<tbody>
					@foreach($roles as $role)
					<tr>
						<td>{{$loop->iteration}}</td>
						<td>{{$role->name}}</td>
						@if($role->permissions->count() > 0)
						<td>
							<ul style="padding-left: 15px;list-style-type: none;">
								@php
								foreach ($role->permissions as $key => $permission) {
									$name = str_ireplace(['view ', 'create ', 'edit ', 'delete ', 'reset password '], '', $permission->name);
									$name = trim($name);
									if (Str::contains(strtolower($permission->name), 'laporan')) {
										$items['laporan'][] = $permission;
									}elseif (Str::contains($permission->name, $name)) {
										$items[$name][] = $permission;
									}
								}
								@endphp
								@foreach($items as $name => $item)
								<li class="mt-2">
									<span class="text-primary">{{ucwords($name)}}</span>
									<br>
									@foreach($item as $v)
									<span class="badge badge-secondary mr-2 mt-2 p-1">{{ucwords($v->name)}}</span>
									@endforeach
								</li>
								@endforeach
							</ul>
						</td>
						@else
						<td><center><i>No Item</i></center></td>
						@endif
						@canany(['edit hak akses', 'delete hak akses'])
						<td class="text-nowrap">
							<button class="btn btn-sm btn-warning" data-id="{{$role->id}}" data-title="{{$role->name}}" data-roles="{{$role->permissions->pluck('name')->toJson()}}" onclick="modalEdit(this)"><i class="fa fa-edit"></i></button>
							&nbsp;&nbsp;
							<button class="btn btn-sm btn-danger" data-id="{{$role->id}}" data-title="{{$role->name}}" onclick="modalDelete(this)"><i class="fa fa-trash"></i></button>
						</td>
						@endcan
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>

@can('create hak akses')
<div class="modal fade" id="modal-create" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form action="{{route('dashboard.role.store')}}" method="POST">
				@csrf
				<div class="modal-header bg-primary">
					<h5 class="modal-title text-white">Tambah Hak Akses</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama Akses</label>
						<input type="text" name="name" class="form-control" autocomplete="off" required>
					</div>
					<div class="form-group">
						<label>Hak Akses</label>
						<br>
						<ul style="list-style-type: none;">
							@foreach($nameRoles as $name => $nameRole)
							<li class="border-top py-2">
								<b>{{ucwords($name)}}</b>
								<ul class="list-roles">
									@foreach($nameRole as $role)
									<li>
										<div class="form-check">
											<label class="form-check-label"><input class="form-check-input" data-role="{{strtolower($name)}}" onclick="setPermissions(this)" type="checkbox" name="permissions[]" value="{{strtolower($role->name)}}"> {{ucwords($role->name)}}</label>
										</div>
									</li>
									@endforeach
								</ul>
							</li>
							@endforeach
						</ul>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('edit hak akses')
<div class="modal fade" id="modal-edit" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('PUT')
				<div class="modal-header bg-warning">
					<h5 class="modal-title text-white">Edit Hak Akses</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Name</label>
						<input type="text" name="name" autocomplete="off" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Permissions</label>
						<br>
						<ul style="list-style-type: none;">
							@foreach($nameRoles as $name => $nameRole)
							<li class="border-top py-2">
								<b>{{ucwords($name)}}</b>
								<ul class="list-roles">
									@foreach($nameRole as $role)
									<li>
										<div class="form-check">
											<label class="form-check-label"><input class="form-check-input" data-role="{{strtolower($name)}}" onclick="setPermissions(this)" type="checkbox" name="permissions[]" value="{{strtolower($role->name)}}"> {{ucwords($role->name)}}</label>
										</div>
									</li>
									@endforeach
								</ul>
							</li>
							@endforeach
						</ul>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-warning">Upadte</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@can('delete hak akses')
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form action="" method="POST">
				@csrf
				@method('DELETE')
				<div class="modal-header bg-danger">
					<h5 class="modal-title text-white">Hapus Hak Akses</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Are you sure delete role <b id="modal-delete-role"></b></p>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="$(this).parents('.modal').modal('hide')">Close</button>
					<button type="submit" class="btn btn-danger">Delete</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endcan

@endsection
@push('js')
<script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready( function () {
		$('#dataTable').DataTable();
	});

	@canany(['create hak akses', 'edit hak akses'])
	$("#modal-create, #modal-edit").find("form").submit(function(){
		if ($(this).find('input:checkbox').filter(':checked').length < 1){
			alert("Please Check at least one Check Box");
			return false;
		}else{
			$(this).find('input:checkbox').removeAttr('disabled');
		}
	});

	function setPermissions(self)
	{
		var value = $(self).val();
		var role  = $(self).data('role');
		var modal = $(self).parents('.modal');
		if ($(self).is(":checked")) {
			if (!value.includes('view')) {
				modal.find('input[type="checkbox"][value="view '+role+'"]').attr('disabled', true);
				modal.find('input[type="checkbox"][value="view '+role+'"]').prop('checked', 'checked');
			}
		}else{
			if (modal.find('input[type="checkbox"][data-role="'+role+'"]:not(:disabled)').filter(':checked').length < 1) {
				modal.find('input[type="checkbox"][value="view '+role+'"]').removeAttr('disabled');
			}
		}
	}
	@endcan

	@can('edit hak akses')
	function modalEdit(self)
	{
		var id    = $(self).attr('data-id');
		var roles = $(self).attr('data-roles');
		var modal = $('#modal-edit');
		var url   = '{{ route("dashboard.role.update", ":id") }}';
		url = url.replace(':id', id);
		roles = JSON.parse(roles);
		roles.forEach(function(name){
			modal.find('input[type="checkbox"][value="'+name+'"]').prop('checked', 'checked');
		});
		modal.find('input[type="checkbox"][value*="view"]:checked').each(function(){
			var role = $(this).data('role');
			if (modal.find('input[type="checkbox"][data-role="'+role+'"][value!="view '+role+'"]:checked').length < 1) {
				modal.find('input[type="checkbox"][value="view '+role+'"]').removeAttr('disabled');
			}else{
				modal.find('input[type="checkbox"][value="view '+role+'"]').attr('disabled', true);		
			}
		});
		modal.find('form').attr('action', url);
		modal.find('input[name="name"]').val($(self).attr('data-title'));
		modal.modal('show');
	}
	@endcan

	@can('delete hak akses')
	function modalDelete(self)
	{
		var id    = $(self).attr('data-id');
		var modal = $('#modal-delete');
		var url   = '{{ route("dashboard.role.destroy", ":id") }}';
		url = url.replace(':id', id);
		modal.find('form').attr('action', url);
		modal.find('#modal-delete-role').html('('+$(self).attr('data-title')+')');
		modal.modal('show');
	}
	@endcan
</script>
@endpush