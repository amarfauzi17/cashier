@extends('layouts.backend')
@section('content')
<div class="row">
	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-primary shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
						Total Penjualan (Bulanan)</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800">Rp. {{rupiah($bills->sum('total_pay')) ?? 0}}</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-success shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-success text-uppercase mb-1">
						Profit (Bulanan)</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800">Rp. {{rupiah($bills->sum('profit')) ?? 0}}</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Pending Requests Card Example -->
	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-warning shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
						Total Barang</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800">{{$total_items}}</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Earnings (Monthly) Card Example -->
	<div class="col-xl-3 col-md-6 mb-4">
		<div class="card border-left-info shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-info text-uppercase mb-1">Stok Tersedia
						</div>
						<div class="row no-gutters align-items-center">
							<div class="col-auto">
								<div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{$current_stock.' / '.$total_stock}}</div>
							</div>
							<div class="col">
								<div class="progress progress-sm mr-2">
									<div class="progress-bar bg-info" role="progressbar"
									style="width: {{$current_stock / $total_stock * 100}}%" aria-valuenow="{{$current_stock / $total_stock * 100}}" aria-valuemin="0"
									aria-valuemax="100"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-auto">
						<i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Content Row -->

<div class="row">
	<!-- Area Chart -->
	<div class="col-8 col-lg-7">
		<div class="card shadow mb-4">
			<!-- Card Header - Dropdown -->
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Grafik Penghasilan Bulanan</h6>
			</div>
			<!-- Card Body -->
			<div class="card-body">
				<div class="chart-area">
					<canvas id="myAreaChart"></canvas>
				</div>
			</div>
		</div>
	</div>

	<!-- Pie Chart -->
	<div class="col-4 col-lg-5">
		<div class="card shadow mb-4">
			<!-- Card Header - Dropdown -->
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Barang Terlaris</h6>
			</div>
			<!-- Card Body -->
			<div class="card-body">
				<table class="table table-bordered">
					<thead>
						<tr>
							<td><b>Barang</b></td>
							<td><b>Total Terjual</b></td>
						</tr>
					</thead>
					<tbody>
						@forelse($products as $product)
						<tr>
							<td>{{$product->product->name}}</td>
							<td>{{$product->total_amount}}</td>
						</tr>
						@empty
						<tr>
							<td colspan="2"><center><i>No Item</i></center></td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection
@push('js')
<script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script> 
<script src="{{ asset('vendor/chart.js/Chart.bundle.min.js') }}"></script>
<script type="text/javascript">
	const ctx = document.getElementById('myAreaChart').getContext('2d');
	const myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: {!! json_encode($periode) !!},
			datasets: [
			{
				label: '# Pendapatan',
				data: {!! json_encode($chartData['total_pay']) !!},
				borderColor: [
				'rgba(255, 99, 132, 1)'
				],
				borderWidth: 1
			},
			{
				label: '# Profit',
				data: {!! json_encode($chartData['profit']) !!},
				borderColor: ['rgba(0, 123, 255, 1)'],
				borderWidth: 1
			}
			]
		},
		options: {
			scales: {
				y: {
					beginAtZero: false
				},
				yAxes: [{
					ticks: {
						beginAtZero:true,
						min: 0,
						max: {{max($chartData['total_pay'])}}
					}
				}]
			},
		}
	});
</script>
@endpush