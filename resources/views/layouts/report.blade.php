<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Report @yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/datatable/jquery.dataTables.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('vendor/datatable/buttons.dataTables.min.css')}}">
	@yield('css')
</head>
<body>
	<div class="p-2 container-fluid">
		@yield('content')
	</div>
	<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
	<script type="text/javascript" src="{{asset('vendor/datatable/jquery-3.5.1.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('vendor/datatable/jquery.dataTables.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('vendor/datatable/dataTables.buttons.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('vendor/datatable/jszip.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('vendor/datatable/pdfmake.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('vendor/datatable/vfs_fonts.js')}}"></script>
	<script type="text/javascript" src="{{asset('vendor/datatable/buttons.html5.min.js')}}"></script>
	@stack('js')
</body>
</html>