<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->string('id', 20)->primary();
            $table->decimal('pay', 9, 3)->nullable();
            $table->integer('discount')->nullable();
            $table->decimal('cutting_price', 9, 3)->nullable();
            $table->decimal('total_pay', 9, 3)->nullable();
            $table->decimal('pay_money', 9, 3)->nullable();
            $table->decimal('profit', 9, 3)->nullable();
            $table->string('customer_id')->nullable();
            $table->integer('status')->default(1)->comments('1 = draft, 2 = checkout');
            $table->timestamps();
            
            $table->foreign('customer_id')->references('id')->on('customers')->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table('bills', function (Blueprint $table) {
            $table->unsignedBigInteger('created_by')->after('status')->nullable();
            $table->unsignedBigInteger('updated_by')->after('created_at')->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign('updated_by')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bills', function (Blueprint $table) {
            $table->dropForeign(['customer_id']);
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);
        });
        Schema::dropIfExists('bills');
    }
};
