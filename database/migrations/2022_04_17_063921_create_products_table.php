<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->string('id', 20)->primary();
            $table->string('name')->nullable()->unique();
            $table->string('thumbnail')->nullable();
            $table->integer('current_stock')->default(0);
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('unit_id')->nullable();
            $table->decimal('purchase_price', 9, 3)->nullable();
            $table->decimal('selling_price', 9, 3)->nullable();
            $table->timestamps();

            $table->foreign('unit_id')->references('id')->on('units')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('CASCADE')->onDelete('SET NULL');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->unsignedBigInteger('created_by')->after('selling_price')->nullable();
            $table->unsignedBigInteger('updated_by')->after('created_at')->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
            $table->foreign('updated_by')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign(['unit_id']);
            $table->dropForeign(['category_id']);
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);
        });
        Schema::dropIfExists('products');
    }
};
