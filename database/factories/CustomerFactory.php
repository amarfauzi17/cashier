<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Customer;
use App\Models\User;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Customer>
 */
class CustomerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Customer::class;

    public function definition()
    {
        $user = User::inRandomOrder()->first();
        return [
            'id'         => $this->faker->numerify('##########'),
            'name'       => $this->faker->name,
            'phone'      => preg_replace("/[^0-9]/", "",$this->faker->phoneNumber),
            'address'    => $this->faker->address,
            'created_by' => $user->id,
            'updated_by' => $user->id,
        ];
    }
}
