<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Supplier;
use App\Models\User;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Supplier>
 */
class SupplierFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Supplier::class;

    public function definition()
    {
        $user = User::inRandomOrder()->first();
        return [
            'name'       => $this->faker->company,
            'phone'      => preg_replace("/[^0-9]/", "",$this->faker->phoneNumber),
            'email'      => $this->faker->unique()->safeEmail(),
            'address'    => $this->faker->address,
            'created_by' => $user->id,
            'updated_by' => $user->id,
        ];
    }
}
