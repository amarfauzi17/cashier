<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\user;
use App\Models\Category;
use Carbon\Carbon;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user = User::inRandomOrder()->first();
        $data = [
        	[
        		'name'		 => 'Sembako',
        		'created_by' => $user->id,
        		'updated_by' => $user->id,
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now(),
        	],
        	[
        		'name'		 => 'Minuman',
        		'created_by' => $user->id,
        		'updated_by' => $user->id,
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now(),
        	],
        	[
        		'name'		 => 'Snack',
        		'created_by' => $user->id,
        		'updated_by' => $user->id,
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now(),
        	],
        	[
        		'name'		 => 'Es Krim',
        		'created_by' => $user->id,
        		'updated_by' => $user->id,
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now(),
        	],
        ];
        Category::insert($data);
    }
}
