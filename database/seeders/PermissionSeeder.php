<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'view kategori']);
        Permission::create(['name' => 'create kategori']);
        Permission::create(['name' => 'edit kategori']);
        Permission::create(['name' => 'delete kategori']);

        Permission::create(['name' => 'view customer']);
        Permission::create(['name' => 'create customer']);
        Permission::create(['name' => 'edit customer']);
        Permission::create(['name' => 'delete customer']);

        Permission::create(['name' => 'view transaksi']);
        Permission::create(['name' => 'create transaksi']);
        Permission::create(['name' => 'delete transaksi']);

        Permission::create(['name' => 'view barang']);
        Permission::create(['name' => 'create barang']);
        Permission::create(['name' => 'edit barang']);
        Permission::create(['name' => 'delete barang']);

        Permission::create(['name' => 'view laporan stok']);
        Permission::create(['name' => 'view laporan transaksi']);
        Permission::create(['name' => 'view laporan barang']);

        Permission::create(['name' => 'view hak akses']);
        Permission::create(['name' => 'create hak akses']);
        Permission::create(['name' => 'edit hak akses']);
        Permission::create(['name' => 'delete hak akses']);

        Permission::create(['name' => 'view stok']);
        Permission::create(['name' => 'create stok']);
        Permission::create(['name' => 'edit stok']);
        Permission::create(['name' => 'delete stok']);

        Permission::create(['name' => 'view supplier']);
        Permission::create(['name' => 'create supplier']);
        Permission::create(['name' => 'edit supplier']);
        Permission::create(['name' => 'delete supplier']);

        Permission::create(['name' => 'view satuan']);
        Permission::create(['name' => 'create satuan']);
        Permission::create(['name' => 'edit satuan']);
        Permission::create(['name' => 'delete satuan']);

        Permission::create(['name' => 'view user']);
        Permission::create(['name' => 'create user']);
        Permission::create(['name' => 'edit user']);
        Permission::create(['name' => 'delete user']);
        Permission::create(['name' => 'reset password user']);

    	$cashierRole = Role::create(['name' => 'Cashier']);
    	$cashierRole->givePermissionTo('view transaksi');
    	$cashierRole->givePermissionTo('create transaksi');
    	$cashierRole->givePermissionTo('delete transaksi');
        $cashierRole->givePermissionTo('view laporan transaksi');

    	$adminGudangRole = Role::create(['name' => 'Admin Gudang']);
        $adminGudangRole->givePermissionTo('view barang');
        $adminGudangRole->givePermissionTo('create barang');
        $adminGudangRole->givePermissionTo('edit barang');
        $adminGudangRole->givePermissionTo('delete barang');
        $adminGudangRole->givePermissionTo('view supplier');
        $adminGudangRole->givePermissionTo('create supplier');
        $adminGudangRole->givePermissionTo('edit supplier');
        $adminGudangRole->givePermissionTo('delete supplier');
        $adminGudangRole->givePermissionTo('view satuan');
        $adminGudangRole->givePermissionTo('create satuan');
        $adminGudangRole->givePermissionTo('edit satuan');
        $adminGudangRole->givePermissionTo('delete satuan');
        $adminGudangRole->givePermissionTo('view stok');
        $adminGudangRole->givePermissionTo('create stok');
        $adminGudangRole->givePermissionTo('edit stok');
        $adminGudangRole->givePermissionTo('delete stok');
        $adminGudangRole->givePermissionTo('view customer');
        $adminGudangRole->givePermissionTo('create customer');
        $adminGudangRole->givePermissionTo('edit customer');
        $adminGudangRole->givePermissionTo('delete customer');
        $adminGudangRole->givePermissionTo('view kategori');
        $adminGudangRole->givePermissionTo('create kategori');
        $adminGudangRole->givePermissionTo('edit kategori');
        $adminGudangRole->givePermissionTo('delete kategori');
        $adminGudangRole->givePermissionTo('view laporan barang');
        $adminGudangRole->givePermissionTo('view laporan stok');

    	User::all()->each(function($user) {
    		$roles = ['Admin Gudang', 'Cashier'];
            $user->assignRole($roles[array_rand($roles)]);
        });

    	$superadminRole = Role::create(['name' => 'superadmin']);

    	$user = User::factory()->create([
    		'name'     => 'superadmin',
    		'email'    => 'superadmin@mail.com',
    		'password' => bcrypt('secret')
    	]);
    	$user->assignRole($superadminRole);
    }
}
