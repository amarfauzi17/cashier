<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\user;
use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Support\Facades\File;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user = User::inRandomOrder()->first();
    	$data = [
    		[
    			'name'     => 'Beras Ros Brand',
                'thumbnail' => '1.jfif',
    			'category_id' => 1,
    			'unit_id' => 1,
    			'purchase_price' => 5000,
    			'selling_price' => 6500,
    			'created_by' => $user->id,
    			'updated_by' => $user->id,
    		],
    		[
    			'name'     => 'Energen',
                'thumbnail' => '2.jpg',
    			'category_id' => 2,
    			'unit_id' => 5,
    			'purchase_price' => 10000,
    			'selling_price' => 12000,
    			'created_by' => $user->id,
    			'updated_by' => $user->id,
    		],
    		[
    			'name'     => 'Oreo',
                'thumbnail' => '3.jpeg',
    			'category_id' => 3,
    			'unit_id' => 3,
    			'purchase_price' => 400,
    			'selling_price'  => 500,
    			'created_by' => $user->id,
    			'updated_by' => $user->id,
    		],
    		[
    			'name'     => 'Minyak Goreng Bimoli',
                'thumbnail' => '4.png',
    			'category_id' => 1,
    			'unit_id' => 4,
    			'purchase_price' => 14000,
    			'selling_price'  => 17000,
    			'created_by' => $user->id,
    			'updated_by' => $user->id,
    		],
            [
                'name'     => 'Bertolli Extra Light Olive Oil',
                'thumbnail' => '5.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 75000,
                'selling_price'  => 80000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
            [
                'name'     => 'Borges Sunflower High Oleic Oil',
                'thumbnail' => '6.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 70000,
                'selling_price'  => 74000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
            [
                'name'     => 'Sania Premium Cooking Oil',
                'thumbnail' => '7.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 18000,
                'selling_price'  => 23000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
            [
                'name'     => 'Barco Coconut Cooking Oil',
                'thumbnail' => '8.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 50000,
                'selling_price'  => 65000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
            [
                'name'     => 'Rose Brand Minyak Goreng',
                'thumbnail' => '9.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 15000,
                'selling_price'  => 18000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
            [
                'name'     => 'Happy Soya Oil',
                'thumbnail' => '10.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 45000,
                'selling_price'  => 50000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
            [
                'name'     => 'Minyak Jagung MamaSuka',
                'thumbnail' => '11.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 45000,
                'selling_price'  => 50000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
            [
                'name'     => 'Minyak Goreng Tawon',
                'thumbnail' => '12.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 45000,
                'selling_price'  => 50000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
            [
                'name'     => 'Beras BMW',
                'thumbnail' => '13.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 10000,
                'selling_price'  => 18000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
            [
                'name'     => 'Beras Cap Bunga',
                'thumbnail' => '14.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 30000,
                'selling_price'  => 35000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
            [
                'name'     => 'Beras Rojolele',
                'thumbnail' => '15.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 30000,
                'selling_price'  => 35000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
            [
                'name'     => 'Beras Sania',
                'thumbnail' => '16.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 50000,
                'selling_price'  => 55000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
            [
                'name'     => 'Beras Maknyus',
                'thumbnail' => '17.png',
                'category_id' => 1,
                'unit_id' => 4,
                'purchase_price' => 55000,
                'selling_price'  => 65000,
                'created_by' => $user->id,
                'updated_by' => $user->id,
            ],
    	];

    	foreach ($data as $item) {
            $supplier = Supplier::inRandomOrder()->first();
            $stock    = rand(1,10);
            $item['current_stock'] = $stock;
            $product  = Product::create($item);
            if ($product) {
                if (File::exists(storage_path('app/img/barang/'.$item['thumbnail']))) {
                    File::copy(storage_path('app/img/barang/'.$item['thumbnail']), public_path('img/barang/'.$item['thumbnail']));
                }
                $product->stocks()->create([
                    'stock'       => $stock,
                    'supplier_id' => $supplier->id,
                    'created_by'  => $user->id,
                    'updated_by'  => $user->id
                ]);
            }
        }
    }
}
