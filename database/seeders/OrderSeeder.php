<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Bill;
use App\Models\Customer;
use App\Models\Product;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Bill::unguard();
    	$dates = displayDates(date('Y-m-d', strtotime('-1 week')), date('Y-m-d'));
    	foreach ($dates as $date) {
    		$jum = rand(1, 7);
    		for ($i=1; $i <= $jum; $i++) {
    			$products = Product::where('current_stock', '>', 0)->inRandomOrder()->take(rand(1,5))->get();
    			$random   = ['a', 'b'];
    			$pay 	  = $products->sum('selling_price'); 
    			$rand     = $random[array_rand($random)];
    			if ($rand == 'a') {
    				$discount = null;
    				$cutPrice = null;
    				$totalPay = $pay;
    				$customer = null;
    			}else{
    				$discount = rand(5, 20);
    				$cutPrice = $pay * $discount / 100;
    				$totalPay = $pay - $cutPrice;
    				$customer = Customer::inRandomOrder()->first()->id;
    			}
    			$discount = $rand == 'a' ? null : rand(5, 20);
    			$payMoney = $this->roundUpCurrency($totalPay);
    			$profit   = $totalPay - $products->sum('purchase_price');
    			$dated    = $date.' '.rand(1,24).':'.rand(1,60).':'.rand(1,60);
    			$bill = [
    				'pay'           => $pay,
    				'discount'      => $discount,
    				'cutting_price' => $cutPrice,
    				'total_pay'     => $totalPay,
    				'pay_money'     => $payMoney,
    				'profit'	    => $profit,
    				'customer_id'	=> $customer,
    				'status'   		=> 2,
    				'created_at'    => $dated,
    				'updated_at'    => $dated,
    			];
    			$cBill = Bill::create($bill);
    			if ($cBill) {
    				foreach ($products as $product) {
    					$order = [
    						'product_id' => $product->id,
    						'amount'	 => 1,
    						'created_at' => $dated,
    						'updated_at' => $dated,
    					];
    					$cOrder = $cBill->orders()->create($order);
    					if ($cOrder) {
    						$cOrder->product()->decrement('current_stock');
    					}
    				}
    			}
    		}
    	}
    	Bill::reguard();
    }

    private function roundUpCurrency($value)
    {
    	$x = 1;
    	$roundup = $result = (float)50000;
    	do {
    		if ($value <= $result) {
    			return $result;
    		}else{
    			$result += $roundup;
    		}
    	} while ( $x <= 1);
    }
}
