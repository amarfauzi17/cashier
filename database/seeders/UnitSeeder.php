<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\user;
use App\Models\Unit;
use Carbon\Carbon;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::inRandomOrder()->first();
        $data = [
        	[
        		'name'		 => 'Kg',
        		'created_by' => $user->id,
        		'updated_by' => $user->id,
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now(),
        	],
        	[
        		'name'		 => 'Dus',
        		'created_by' => $user->id,
        		'updated_by' => $user->id,
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now(),
        	],
        	[
        		'name'		 => 'Satuan',
        		'created_by' => $user->id,
        		'updated_by' => $user->id,
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now(),
        	],
        	[
        		'name'		 => 'Liter',
        		'created_by' => $user->id,
        		'updated_by' => $user->id,
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now(),
        	],
        	[
        		'name'		 => 'Renteng',
        		'created_by' => $user->id,
        		'updated_by' => $user->id,
        		'created_at' => Carbon::now(),
        		'updated_at' => Carbon::now(),
        	],
        ];
        Unit::insert($data);
    }
}
