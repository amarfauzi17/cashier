<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Unit;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('permission:view satuan', ['only' => ['index']]);
        $this->middleware('permission:create satuan', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit satuan', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete satuan', ['only' => ['destroy']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        $units = Unit::orderByDesc('created_at')->get();
        return view('backend.unit.index', compact('units'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:units,name'
        ]);
        $unit = Unit::create(['name' => strip_tags($request->name)]);
        if ($unit) {
            return back()->with('success','Input satuan barang berhasil');
        }
        return back()->with('error','Input satuan barang gagal, silakan coba kembali!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unit = Unit::find($id);
        if ($unit) {
            $request->validate([
                'name' => 'required|unique:units,name,'.$unit->id
            ]);
            $upd = $unit->update(['name' => strip_tags($request->name)]);
            if ($upd) {
                return back()->with('success','Update satuan barang berhasil');
            }
            return back()->with('error','Update satuan barang gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit = Unit::find($id);
        if ($unit) {
            $del = $unit->delete();
            if ($del) {
                return back()->with('success','Hapus satuan barang berhasil');
            }
            return back()->with('error','Hapus satuan barang gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }
}
