<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Supplier;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('permission:view supplier', ['only' => ['index']]);
        $this->middleware('permission:create supplier', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit supplier', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete supplier', ['only' => ['destroy']]);
    }

    public function index()
    {
        $suppliers = Supplier::orderByDesc('created_at')->get();
        return view('backend.supplier.index', compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'    => 'required|unique:suppliers,name',
            'phone'   => 'required|numeric|digits_between:8,15',
            'email'   => 'required|email',
            'address' => 'required'
        ]);
        $payloads = removeHtmlTagsOfFields($request->except('_token'));
        $supplier = Supplier::create($payloads);
        if ($supplier) {
            return back()->with('success','Input supplier berhasil');
        }
        return back()->with('error','Input supplier gagal, silakan coba kembali!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::find($id);
        if ($supplier) {
            $request->validate([
                'name'    => 'required|unique:suppliers,name',
                'phone'   => 'required|numeric|digits_between:8,15',
                'email'   => 'required|email',
                'address' => 'required'
            ]);
            $payloads = removeHtmlTagsOfFields($request->except('_token', '_method'));
            $update   = $supplier->update($payloads);
            if ($update) {
                return back()->with('success','Update supplier berhasil');
            }
            return back()->with('error','Update supplier gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        if ($supplier) {
            $del = $supplier->delete();
            if ($del) {
                return back()->with('success','Hapus supplier berhasil');
            }
            return back()->with('error','Hapus supplier gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }
}
