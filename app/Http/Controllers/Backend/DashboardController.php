<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bill;
use App\Models\Product;
use App\Models\Stock;
use App\Models\Order;
use DatePeriod;
use DateTime;
use DateInterval;

class DashboardController extends Controller
{
	public function index()
	{
		$bills 		   = Bill::whereMonth('created_at', date('m'))->get();
		$total_stock   = (int)Stock::sum('stock');
		$total_items   = Product::select('id')->count();
		$current_stock = (int)Product::sum('current_stock');
		$products      = Order::with('product')->selectRaw("product_id, SUM(amount) as total_amount")->groupBy('product_id')->orderByDesc('amount')->take(5)->get();
		$items         = Bill::whereBetween('created_at', [date('Y-m-d', strtotime('-1 week'))." 00:00:00", date('Y-m-d')." 23:59:59"])->get()->groupBy(function($item) {
			return date('d-m-Y', strtotime($item->created_at));
		});
		$periode = displayDates(date('Y-m-d', strtotime('-1 week')), date('Y-m-d'));
		$chartData['total_pay'] = [];
		$chartData['profit']    = [];
		foreach ($periode as $day) {
			if (isset($items[$day])) {
				$chartData['total_pay'][] = $items[$day]->sum('total_pay');
				$chartData['profit'][]    = $items[$day]->sum('profit');
			}else{
				$chartData['total_pay'][] = 0;				
				$chartData['profit'][]    = 0;				
			}
		}
		return view('backend.dashboard', compact('bills', 'total_items', 'total_stock', 'current_stock', 'products', 'chartData', 'periode'));
	}
}
