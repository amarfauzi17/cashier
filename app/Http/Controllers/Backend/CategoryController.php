<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('permission:view kategori', ['only' => ['index']]);
        $this->middleware('permission:create kategori', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit kategori', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete kategori', ['only' => ['destroy']]);
    }

    public function index()
    {
        $categories = Category::orderByDesc('created_at')->get();
        return view('backend.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:categories,name'
        ]);
        $category = Category::create(['name' => strip_tags($request->name)]);
        if ($category) {
            return back()->with('success','Input kategori barang berhasil');
        }
        return back()->with('error','Input kategori barang gagal, silakan coba kembali!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        if ($category) {
            $request->validate([
                'name' => 'required|unique:categories,name,'.$category->id
            ]);
            $upd = $category->update(['name' => strip_tags($request->name)]);
            if ($upd) {
                return back()->with('success','Update kategori barang berhasil');
            }
            return back()->with('error','Update kategori barang gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        if ($category) {
            $del = $category->delete();
            if ($del) {
                return back()->with('success','Hapus kategori barang berhasil');
            }
            return back()->with('error','Hapus kategori barang gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }
}
