<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Stock;
use App\Models\Supplier;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('permission:view stok', ['only' => ['index']]);
        $this->middleware('permission:create stok', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit stok', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete stok', ['only' => ['destroy']]);
    }

    public function index()
    {
        $stocks    = Stock::with('product', 'supplier')->orderByDesc('created_at')->get();
        $suppliers = Supplier::orderBy('name')->get();
        return view('backend.stock.index', compact('stocks', 'suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_id'  => 'required|exists:products,id',
            'stock'       => 'required|numeric',
            'supplier_id' => 'required|exists:suppliers,id',
        ]);
        $payloads = $request->except('_token');
        $payloads = removeHtmlTagsOfFields($payloads);
        $stock    = Stock::create($payloads);
        if ($stock) {
            $stock->product()->update(['current_stock' => $stock->product->current_stock + $request->stock]);
            return back()->with('success','Input stok barang berhasil');
        }
        return back()->with('error','Input stok barang gagal, silakan coba kembali!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stock = Stock::find($id);
        if ($stock) {
            $request->validate([
                'product_id'  => 'required|exists:products,id',
                'stock'       => 'required|numeric',
                'supplier_id' => 'required|exists:suppliers,id',
            ]);
            $oldStock = $stock->stock;
            $payloads = $request->except('_token', '_method');
            $payloads = removeHtmlTagsOfFields($payloads);
            $upd      = $stock->update($payloads);
            if ($upd) {
                // dd($stock->product->current_stock, $request->stock, $oldStock);
                $stock->product()->update(['current_stock' => $stock->product->current_stock + $request->stock - $oldStock]);
                return back()->with('success','Update stok barang berhasil');
            }
            return back()->with('error','Update stok barang gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stock = Stock::find($id);
        if ($stock) {
            $delete = $stock->delete();
            if ($delete) {
                $stock->product()->update(['current_stock' => $stock->product->current_stock - $stock->stock]);
                return back()->with('success','Hapus stok barang berhasil');
            }
            return back()->with('error','Hapus stok barang gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }
}
