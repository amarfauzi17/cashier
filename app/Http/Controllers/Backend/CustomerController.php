<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('permission:view customer', ['only' => ['index']]);
        $this->middleware('permission:create customer', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit customer', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete customer', ['only' => ['destroy']]);
    }

    public function index()
    {
        $customers = Customer::orderByDesc('created_at')->get();
        return view('backend.customer.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id'      => 'required|numeric|unique:customers,id',
            'name'    => 'required',
            'phone'   => 'required|numeric|digits_between:8,15',
            'address' => 'required'
        ]);
        $payloads = removeHtmlTagsOfFields($request->except('_token'));
        $customer = Customer::create($payloads);
        if ($customer) {
            return back()->with('success','Input customer berhasil');
        }
        return back()->with('error','Input customer gagal, silakan coba kembali!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        if ($customer) {
            $request->validate([
                'id'      => 'required|numeric|unique:customers,id,'.$customer->id,
                'name'    => 'required',
                'phone'   => 'required|numeric|digits_between:8,15',
                'address' => 'required'
            ]);
            $payloads = removeHtmlTagsOfFields($request->except('_token', '_method'));
            $customer = $customer->update($payloads);
            if ($customer) {
                return back()->with('success','Update customer berhasil');
            }
            return back()->with('error','Update customer gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        if ($customer) {
            $del = $customer->delete();
            if ($del) {
                return back()->with('success','Hapus customer berhasil');
            }
            return back()->with('error','Hapus customer gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }
}
