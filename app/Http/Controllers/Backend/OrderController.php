<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Bill;
use App\Models\Order;
use App\Models\Product;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('permission:view transaksi', ['only' => ['index', 'show', 'receipt']]);
        $this->middleware('permission:create transaksi', ['only' => ['store', 'update']]);
        $this->middleware('permission:delete transaksi', ['only' => ['destroy']]);
    }

    public function index()
    {
        $bills = Bill::with('customer')->orderByDesc('created_at')->get();
        return view('backend.order.index', compact('bills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_id' => 'required|exists:products,id'
        ]);

        $product = Product::where([['id', '=', $request->product_id], ['current_stock', '>', 0]])->first();
        if ($product) {
            # code...
            if ($bill = Bill::with('orders.product')->find($request->id)) {
                $order = $bill->orders->where('product_id', $request->product_id)->first();
                if ($order) {
                    $max_stock = $order->product->stocks()->sum('stock');
                    if ((int)($order->amount + 1) <= (int)$max_stock) {
                        $bill->update(['pay' => $bill->pay + $product->selling_price]);
                        $order->increment('amount');
                        $product->decrement('current_stock');
                    }
                }else{
                    $bill->update(['pay' => $bill->pay + $product->selling_price]);
                    $bill->orders()->create([
                        'product_id' => $request->product_id,
                        'amount'     => 1,
                    ]);
                    $product->decrement('current_stock');
                }
            }else{
                $bill = Bill::create(['pay' => $product->selling_price, 'status' => 1]);
                $bill->orders()->create([
                    'product_id' => $request->product_id,
                    'amount'     => 1,
                ]);
                $product->decrement('current_stock');
            }
            return redirect()->route('dashboard.order.show', $bill->id);
        }
        return redirect()->back()->with('error', 'Stok barang tidak tersedia');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = null)
    {
        $customers = Customer::orderBy('name')->get();
        $noInv     = $id;
        $bill      = Bill::select('status', 'customer_id')->where(['status' => 2, 'id' => $id])->first();
        return view('backend.order.show', compact('customers', 'noInv', 'bill'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bill = Bill::where(['status' => 1, 'id' => $id])->first();
        if ($bill) {
            $cprice = $bill->pay * $bill->discount / 100;
            $total  = $bill->pay - $cprice;
            $nomi   = 0;
            $fund   = $bill->orders->map(function($order) use ($nomi){
                return $nomi += ($order->product->purchase_price * $order->amount);
            })->first();
            $profit = $total - (float)$fund;
            $bill->update([
                'cutting_price' => $cprice,
                'total_pay'     => $total,
                'profit'        => $profit,
                'customer_id'   => $request->customer_id,
                'status'        => 2
            ]);
            return redirect()->back()->with('success', 'Transaksi Berhasil');
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bill = Bill::where(['id' => $id, 'status' => 1])->first();
        if ($bill) {
            $del = $bill->delete();
            if ($del) {
                $bill->orders->map(function($order){
                    $order->product()->increment('current_stock', $order->amount);
                });
                return back()->with('success','Hapus transaksi dengan nomor struk '.$bill->id.' berhasil');
            }
            return back()->with('error','Hapus transaksi dengan nomor struk '.$bill->id.' gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }

    public function receipt($id)
    {
        $bill = Bill::with('orders.product', 'createdBy')->where(['id' => $id, 'status' => 2])->first();
        if ($bill) {
            return view('backend.order.receipt', compact('bill'));
        }
        return redirect()->route('dashboard.error.404');
    }
}
