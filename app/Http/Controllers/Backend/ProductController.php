<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Unit;
use App\Models\Supplier;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('permission:view barang', ['only' => ['index']]);
        $this->middleware('permission:create barang', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit barang', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete barang', ['only' => ['destroy']]);
    }

    public function index()
    {
        $products = Product::with('category', 'stocks', 'unit')->orderByDesc('created_at')->get();
        return view('backend.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name')->get();
        $units      = Unit::orderBy('name')->get();
        $suppliers  = Supplier::orderBy('name')->get();
        return view('backend.product.create', compact('categories', 'units', 'suppliers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'           => 'required|unique:products,name',
            'thumbnail'      => 'nullable|dimensions:max_width=1024,max_height=1024|max:1024|image',
            'category_id'    => 'required|exists:categories,id',
            'unit_id'        => 'required|exists:units,id',
            'stock'          => 'required|numeric',
            'purchase_price' => 'required',
            'selling_price'  => 'required',
        ]);

        $purchase = trim(str_ireplace(['rp.', '.'], '', $request->purchase_price));
        $purchase = (double)str_replace(',', '.', $purchase);
        $purchase = round($purchase, 3);

        $selling = trim(str_ireplace(['rp.', '.'], '', $request->selling_price));
        $selling = (double)str_replace(',', '.', $selling);
        $selling = round($selling, 3);
        
        $payloads = $request->except('_token', 'selling_price', 'purchase_price', 'stock', 'thumbnail', 'supplier');
        $payloads['purchase_price'] = $purchase;
        $payloads['selling_price']  = $selling;
        $payloads['current_stock']  = $request->stock;

        if($request->hasFile('thumbnail')){
            $file = $request->file('thumbnail');
            $ext  = $file->getClientOriginalExtension();
            $name = uniqid().'.'.$ext;
            $file->move(public_path('img/barang'), $name);
            $payloads['thumbnail'] = $name;
        }
        $payloads = removeHtmlTagsOfFields($payloads);
        $product  = Product::create($payloads);
        if ($product) {
            $product->stocks()->create(['stock' => $request->stock, 'supplier_id' =>  $request->supplier]);
            return redirect()->route('dashboard.product.index')->with('success','Input barang berhasil');
        }
        return back()->with('error','Input barang gagal, silakan coba kembali!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        if ($product) {
            $categories = Category::orderBy('name')->get();
            $units      = Unit::orderBy('name')->get();
            $suppliers  = Supplier::orderBy('name')->get();
            return view('backend.product.edit', compact('categories', 'units', 'suppliers', 'product'));
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if ($product) {
            $request->validate([
                'name'           => 'required|unique:products,name,'.$product->id,
                'thumbnail'      => 'nullable|dimensions:max_width=1024,max_height=1024|max:1024|image',
                'category_id'    => 'required|exists:categories,id',
                'purchase_price' => 'required',
                'selling_price'  => 'required',
            ]);

            $purchase = trim(str_ireplace(['rp.', '.'], '', $request->purchase_price));
            $purchase = (double)str_replace(',', '.', $purchase);
            $purchase = round($purchase, 3);

            $selling = trim(str_ireplace(['rp.', '.'], '', $request->selling_price));
            $selling = (double)str_replace(',', '.', $selling);
            $selling = round($selling, 3);

            $payloads = $request->except('_token', '_method', 'selling_price', 'purchase_price', 'stock', 'thumbnail', 'supplier');
            $payloads['purchase_price'] = $purchase;
            $payloads['selling_price']  = $selling;

            if($request->hasFile('thumbnail')){
                $file = $request->file('thumbnail');
                $ext  = $file->getClientOriginalExtension();
                $name = uniqid().'.'.$ext;
                $upload = $file->move(public_path('img/barang'), $name);
                if ($upload) {
                    if (File::exists(public_path('img/barang/'.$product->thumbnail))) {
                        File::delete(public_path('img/barang/'.$product->thumbnail));
                    }
                }
                $payloads['thumbnail'] = $name;
            }
            $payloads = removeHtmlTagsOfFields($payloads);
            $product  = $product->update($payloads);
            if ($product) {
                return redirect()->route('dashboard.product.index')->with('success','Update barang berhasil');
            }
            return back()->with('error','Update barang gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if ($product) {
            $delete = $product->delete();
            if ($delete) {
                if (File::exists(public_path('img/barang/'.$product->thumbnail))) {
                    File::delete(public_path('img/barang/'.$product->thumbnail));
                }
                return back()->with('success','Hapus barang berhasil');
            }
            return back()->with('error','Hapus barang gagal, silakan coba kembali!');
        }
        return redirect()->route('dashboard.error.404');
    }
}
