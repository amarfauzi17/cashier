<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 

    public function __construct()
    {
        $this->middleware('permission:view hak akses', ['only' => ['index']]);
        $this->middleware('permission:create hak akses', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit hak akses', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete hak akses', ['only' => ['destroy']]);
    }

    public function index()
    {
        $nameRoles   = [];
        $permissions = Permission::orderBy('created_at')->get();
        $roles = Role::with('permissions')->where('name', '<>', 'superadmin')->orderByDesc('created_at')->get();

        foreach ($permissions as $key => $permission) {
            $name = str_ireplace(['view ', 'create ', 'edit ', 'delete ', 'reset password '], '', $permission->name);
            $name = trim($name);
            if (Str::contains(strtolower($permission->name), 'laporan')) {
                $nameRoles['laporan'][] = $permission;
            }elseif (Str::contains($permission->name, $name)) {
                $nameRoles[$name][] = $permission;
            }
        }
        return view('backend.role.index', compact('roles', 'permissions', 'nameRoles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles,name',
            'permissions' => 'required|array|min:1',
        ]);
        $role = Role::create(['name' => $request->name]);
        if ($role) {
            $role->givePermissionTo($request->permissions);
            return redirect()->back()->with('success', 'roles '.$role->name.' saved successfully');          
        }
        return redirect()->back()->with('error', 'failed, Try Again');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findById($id);
        if ($role) {
            $request->validate([
                'name' => 'required|unique:roles,name,'.$role->id,
                'permissions' => 'required|array|min:1',
            ]);
            $upd = $role->update(['name' => $request->name]);
            if ($upd) {
                $role->syncPermissions($request->permissions);
                return redirect()->back()->with('success', 'roles '.$role->name.' updated successfully'); 
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }
        return redirect()->route('dashboard.error.404');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findById($id);
        if ($role) {
            $delete = $role->delete();
            if ($delete) {
                return redirect()->back()->with('success', 'role '.$role->name.' deleted successfully');
            }
            return redirect()->back()->with('error', 'failed, Try Again');
        }
        return redirect()->route('dashboard.error.404');
    }
}
