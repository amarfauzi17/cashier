<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Stock;
use App\Models\Customer;
use App\Models\Bill;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:view laporan stok', ['only' => ['stockIndex', 'stockShow']]);
        $this->middleware('permission:view laporan transaksi', ['only' => ['orderIndex', 'orderShow']]);
        $this->middleware('permission:view laporan barang', ['only' => ['productShow']]);
    }

    public function stockIndex()
    {
    	$products = Product::orderBy('name')->get();
    	$categories = Category::orderBy('name')->get();
    	return view('backend.report.stock.index', compact('products', 'categories'));
    }

    public function stockShow(Request $request)
    {
    	$stocks = Stock::with('product.category', 'product.unit', 'createdBy', 'supplier')->whereHas('product', function($product) use ($request){
    		return $product->whereHas('category', function($category) use ($request){
    			return $category->whereIn('id', $request->categories);
    		})->where('id', 'LIKE', $request->product);
    	})->whereBetween('created_at', [$request->fdate." 00:00:00", $request->tdate." 23:59:59"])->orderBy('created_at')->get();
    	return view('backend.report.stock.show', compact('stocks'));
    }

    public function orderIndex()
    {
    	$customers = Customer::orderBy('name')->get();
    	return view('backend.report.order.index', compact('customers'));
    }

    public function orderShow(Request $request)
    {
        // dd($request->all());
    	$bills = Bill::with('orders.product.unit')->where('status', 2)->whereBetween('created_at', [$request->fdate." 00:00:00", $request->tdate." 23:59:59"]);
    	if (!empty($request->minpay)) {
    		$bills = $bills->where('total_pay', '>=', $request->minpay);
    	}
    	if (!empty($request->maxpay)) {
    		$bills = $bills->where('total_pay', '<=', $request->maxpay);
    	}
        if ($request->customer != '%') {
            $bills = $bills->where('customer_id', $request->customer);
        }
    	$bills = $bills->get();
    	$tpay  = $tprofit = $tdiscount = 0;
    	return view('backend.report.order.show', compact('bills', 'request', 'tpay', 'tprofit', 'tdiscount'));
    }

    public function productShow()
    {
    	$products = Product::with('unit', 'stocks', 'orders.bill')->orderBy('name')->get();
    	return view('backend.report.product.show', compact('products'));
    }
}
