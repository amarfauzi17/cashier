<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Stock;

class StockController extends Controller
{
	public function searchProduct(Request $request)
	{
		if (!empty($request->q)) {
			$q = '%'.strip_tags($request->q).'%';
			$products = Product::with('unit')->where('id', 'LIKE', $q)->orWhere('name', 'LIKE', $q)->take(10)->get();
			$products = $products->map(function($product){
				return [
					'id'     => $product->id,
					'name'   => $product->name,
					'satuan' => empty($product->unit) ? '' : $product->unit->name,
				];
			});
			return response()->json($products);
		}
	}

	public function edit($id)
	{
		$stock = Stock::select('id', 'product_id', 'supplier_id', 'stock', 'note')->find($id);
		if ($stock) {
			$item = [
				'id'	      => $stock->id,
				'product_id'  => $stock->product_id,
				'supplier_id' => $stock->supplier_id,
				'stock' 	  => $stock->stock,
				'note' 		  => $stock->note,
				'name' 		  => $stock->product->name,
				'unit' 		  => $stock->product->unit->name,
			];
			return response()->json(['status' => 200, 'message' => 'success', 'data' => $item]);
		}
		return response()->json(['status' => 400, 'message' => 'failed']);
	}
}
