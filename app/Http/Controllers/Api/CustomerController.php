<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerController extends Controller
{
    public function edit($id)
	{
		$customer = Customer::select('id', 'name', 'phone', 'address')->find($id);
		if ($customer) {
			return response()->json(['status' => 200, 'message' => 'success', 'data' => $customer]);
		}
		return response()->json(['status' => 400, 'message' => 'failed']);
	}
}
