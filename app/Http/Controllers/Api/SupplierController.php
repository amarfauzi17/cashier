<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Supplier;

class SupplierController extends Controller
{
    public function edit($id)
	{
		$supplier = Supplier::select('id', 'name', 'phone', 'email', 'address')->find($id);
		if ($supplier) {
			return response()->json(['status' => 200, 'message' => 'success', 'data' => $supplier]);
		}
		return response()->json(['status' => 400, 'message' => 'failed']);
	}
}
