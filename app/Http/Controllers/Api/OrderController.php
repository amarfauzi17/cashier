<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\Bill;
use Auth;

class OrderController extends Controller
{
	public function searchProduct(Request $request)
	{
		if (!empty($request->term)) {
			$q = '%'.strip_tags($request->term).'%';
			$products = Product::with('unit')->where('id', 'LIKE', $q)->orWhere('name', 'LIKE', $q)->take(10)->get();
			$products = $products->map(function($product){
				return [
					'value' 		=> $product->id.' '.$product->name,
					'label' 		=> $product->id.' '.$product->name,
					'id' 			=> $product->id,
					'name' 			=> $product->name,
					'thumbnail' 	=> $product->thumbnail,
					'selling_price' => $product->selling_price,
					'unit' 			=> $product->unit->name,
				];
			});
			return response()->json($products);
		}
	}

	public function stock(Request $request)
	{
		if (!empty($request->qty) && !empty($request->order_id) && !empty($request->id)) {
			$order = Order::whereHas('bill', function($q){
				return $q->where('status', 1);
			})->where('id', $request->order_id)->first();
			if ($order) {
				$max_stock = $order->product->current_stock;
				$qty = $request->qty > ($max_stock + $order->amount) ? ($max_stock + $order->amount) : $request->qty;
				if ($order->amount != $qty) {
					$total = ($qty - $order->amount) * $order->product->selling_price;
					$total = $total + $order->bill->pay;
					$order->bill()->update(['pay' => $total]);
				}
				$order->product()->update(['current_stock' => $order->product->current_stock + $order->amount - $qty]);
				$order->update(['amount' => $qty]);
			}
			return $this->data($request);
		}
	}

	public function destroy(Request $request)
	{
		if (!empty($request->order_id) && !empty($request->id)){
			$order = Order::whereHas('bill', function($q){
				return $q->where('status', 1);
			})->where('id', $request->order_id)->first();
			if ($order) {
				$total = $order->product->selling_price * $order->amount;
				$total = $order->bill->pay - $total;
				$order->bill()->update(['pay' => $total]);
				$order->product()->increment('current_stock', $order->amount);
				$order->delete();
			}
			return $this->data($request);
		}
	}

	public function discount(Request $request)
	{
		if (isset($request->id)){
			$bill = Bill::where(['id' => $request->id, 'status' => 1])->first();
			if ($bill) {
				$bill->update([
					'discount' => $request->discount ?? null
				]);
			}
			return $this->data($request);
		}	
	}

	public function paid(Request $request)
	{
		if (isset($request->id)){
			$bill = Bill::where(['id' => $request->id, 'status' => 1])->first();
			if ($bill) {
				$paid = $request->paid;
				if (!empty($paid)) {
					$paid = trim(str_ireplace(['rp.', '.'], '', $paid));
					$paid = (double)str_replace(',', '.', $paid);
					$paid = round($paid, 3);
				}
				$bill->update([
					'pay_money' => $paid
				]);
			}
			return $this->data($request);
		}	
	}

	public function data(Request $request)
	{
		$transaction = Bill::with('orders.product')->find($request->id);
		return view('backend.order.table-transaction', compact('transaction'));
	}

}
