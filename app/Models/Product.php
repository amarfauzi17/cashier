<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use App\Traits\Blameable;

class Product extends Model
{
	use HasFactory, Blameable;
	protected $primary   = 'id';
	public $incrementing = false;

	protected $guarded = [];

	public function createdBy()
	{
		return $this->belongsTo(User::class, 'created_by');
	}

	public function updatedBy()
	{
		return $this->belongsTo(User::class, 'updated_by');
	}

	public function unit()
	{
		return $this->belongsTo(Unit::class);
	}

	public function stocks()
	{
		return $this->hasMany(Stock::class);
	}

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	public function orders()
	{
		return $this->hasMany(Order::class);
	}

	public static function boot()
	{
		parent::boot();
		self::creating(function ($model) {
			$model->id = IdGenerator::generate(['table' => $model->getTable(), 'length' => 15, 'prefix' => 'PRD-'.date('ymd'), 'reset_on_prefix_change' => true]);
		});
	}
}
