<?php

function set_active($uri, $output = 'active')
{
	if( is_array($uri)){
		foreach ($uri as $u) {
			if (Route::is($u)) {
				return $output;
			}
		}
	} else {
		if (Route::is($uri)){
			return $output;
		}
	}
}

function removeHtmlTagsOfFields(array $inputs, array $excepts = [])
{
	$inputOriginal = $inputs;
	$inputs = array_except($inputs, $excepts);
	foreach ($inputs as $index => $in){
		$inputs[$index] = strip_tags($in);
	}
	if(!empty($excepts)){
		foreach ($excepts as $except){
			$inputs[$except] = $inputOriginal[$except];
		}
	}
	return $inputs;
}

function array_except(array $array,array $except) {
	return array_filter($array,fn($key) => !in_array($key,$except),ARRAY_FILTER_USE_KEY);
}

function rupiah($number)
{
	return empty($number) ? null : number_format($number,0,',','.');
}

function displayDates($date1, $date2, $format = 'd-m-Y' ) {
	$dates = array();
	$current = strtotime($date1);
	$date2 = strtotime($date2);
	$stepVal = '+1 day';
	while( $current <= $date2 ) {
		$dates[] = date($format, $current);
		$current = strtotime($stepVal, $current);
	}
	return $dates;
}